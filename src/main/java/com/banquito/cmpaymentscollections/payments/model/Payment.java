package com.banquito.cmpaymentscollections.payments.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@Document(collection = "payments")
@TypeAlias("payments")
@Builder
public class Payment {

  @Id private String id;

  @Indexed(name = "idx_payment_internalId", unique = true)
  private String internalId;

  @Indexed(name = "idx_payment_groupInternalId", unique = false)
  private String groupInternalId;

  private String serviceName;

  private String serviceDescription;

  private String state;

  private BigInteger debtorAccount;

  private Date creationDate;

  private Date lastModifiedDate;

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private Date processDate;

  private Integer records;

  private Integer paidRecords;

  private Integer failedRecords;

  private BigDecimal totalPaymentValue;

  private BigDecimal paidValue;

  private BigDecimal failedValue;

  private String journalId;
}
