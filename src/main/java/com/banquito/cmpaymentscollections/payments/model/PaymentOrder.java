package com.banquito.cmpaymentscollections.payments.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "payment_orders")
@TypeAlias("payment_orders")
@CompoundIndexes({
        @CompoundIndex(
                name = "idxco_paymentOrder_credIdCredIdType",
                def = "{'creditorIdentification': 1, 'creditorIdentificationType': 1}"),
        @CompoundIndex(
                name = "idxco_paymentOrder_credAccCredAccType",
                def = "{'creditorAccount': 1, 'creditorAccountType': 1}")
})
public class PaymentOrder {

  @Id private String id;

  @Indexed(name = "idx_paymentOrder_internalId", unique = true)
  private String internalId;

  @Indexed(name = "idx_paymentOrder_referenceId", unique = false)
  private String referenceId;

  private String paymentId;

  private String creditorIdentification;

  private String creditorIdentificationType;

  private String creditorName;

  private BigInteger creditorAccount;

  private String creditorAccountType;

  private String creditorEmail;

  private BigDecimal amount;

  private String state;

  private String description;

  private Date paymentDate;
}
