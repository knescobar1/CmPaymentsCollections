package com.banquito.cmpaymentscollections.payments.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentDTO {

  private String internalId;

  private String groupInternalId;

  private String serviceName;

  private String serviceDescription;

  private String state;

  private BigInteger debtorAccount;

  private Date processDate;

  private Integer records;

  private Integer paidRecords;

  private Integer failedRecords;

  private BigDecimal totalPaymentValue;

  private BigDecimal paidValue;

  private BigDecimal failedValue;

  private String journalId;
}
