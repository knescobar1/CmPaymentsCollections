package com.banquito.cmpaymentscollections.payments.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DepositDTO implements Serializable {
  private String referenceId;

  private BigInteger accountNumber;

  private BigDecimal amount;

  private String notes;

  private String transactionChannel;

  private String documentNumber;

  private String transactionNumber;
}
