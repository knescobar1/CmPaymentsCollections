package com.banquito.cmpaymentscollections.payments.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentOrderDTO {

  private String internalId;

  private String referenceId;

  private String paymentId;

  private String creditorIdentification;

  private String creditorIdentificationType;

  private String creditorName;

  private BigInteger creditorAccount;

  private String creditorAccountType;

  private String creditorEmail;

  private BigDecimal amount;

  private String state;

  private String description;

  private Date paymentDate;
}
