package com.banquito.cmpaymentscollections.payments.utils;

import java.util.Calendar;
import java.util.Date;

public class SetDateUtils {
    public static Date setTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 1);
        cal.set(Calendar.MINUTE, 1);
        cal.set(Calendar.SECOND, 1);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
