package com.banquito.cmpaymentscollections.payments.resource;

import com.banquito.cmpaymentscollections.payments.dto.PaymentDTO;
import com.banquito.cmpaymentscollections.payments.exception.NotEnoughtFundsException;
import com.banquito.cmpaymentscollections.payments.exception.PaymentServiceException;
import com.banquito.cmpaymentscollections.payments.mapper.PaymentMapper;
import com.banquito.cmpaymentscollections.payments.model.Payment;
import com.banquito.cmpaymentscollections.payments.service.PaymentService;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/payment")
@RequiredArgsConstructor
public class PaymentResource {
  private final PaymentService service;

  @PostMapping
  public ResponseEntity<PaymentDTO> create(@RequestBody PaymentDTO dto) {
    try {
      Payment payment = this.service.create(PaymentMapper.buildPayment(dto));
      return ResponseEntity.ok(PaymentMapper.buildPaymentDTO(payment));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping(path = "/{id}/{mail}")
  public ResponseEntity<PaymentDTO> approve(
      @PathVariable("id") String id, @PathVariable("mail") String mail)
      throws NotEnoughtFundsException, PaymentServiceException, ParseException {
    this.service.approvePayment(id, mail);
    return ResponseEntity.ok().build();
  }

  @GetMapping
  public ResponseEntity<List<PaymentDTO>> obtainPaymentsByProperties(
      @RequestParam String groupId,
      @RequestParam(required = false) String id,
      @RequestParam(required = false) String serviceName,
      @RequestParam(required = false) String state,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate,
      @RequestParam(required = false) String description) {
    try {
      List<PaymentDTO> paymentsDTO =
          this.service
              .obtainPaymentsByFilter(
                  groupId, id, serviceName, state, fromDate, endDate, description)
              .stream()
              .map(payment -> PaymentMapper.buildPaymentDTO(payment))
              .collect(Collectors.toList());
      return ResponseEntity.ok(paymentsDTO);
    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }

  @GetMapping("/state")
  public ResponseEntity<List<PaymentDTO>> obtainPaymentsByState(
      @RequestParam(required = false) String serviceName,
      @RequestParam(required = false) String state,
      @RequestParam String groupId,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate) {
    try {
      List<PaymentDTO> paymentsDTO =
          this.service
              .obtainPaymentsByState(serviceName, state, fromDate, endDate, groupId)
              .stream()
              .map(payment -> PaymentMapper.buildPaymentDTO(payment))
              .collect(Collectors.toList());
      return ResponseEntity.ok(paymentsDTO);
    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }
}
