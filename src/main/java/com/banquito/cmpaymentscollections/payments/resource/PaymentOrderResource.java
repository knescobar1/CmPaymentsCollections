package com.banquito.cmpaymentscollections.payments.resource;

import com.banquito.cmpaymentscollections.payments.dto.PaymentOrderDTO;
import com.banquito.cmpaymentscollections.payments.mapper.PaymentOrderMapper;
import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import com.banquito.cmpaymentscollections.payments.service.PaymentOrderService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "/paymentOrder")
@RequiredArgsConstructor
public class PaymentOrderResource {
  private final PaymentOrderService service;

  @PostMapping("/order/{internalId}")
  public ResponseEntity<PaymentOrderDTO> create(
      @RequestBody PaymentOrderDTO dto, @PathVariable("internalId") String internalId) {
    try {
      PaymentOrder paymentOrder =
          this.service.create(PaymentOrderMapper.buildPaymentOrder(dto), dto.getInternalId());
      return ResponseEntity.ok(PaymentOrderMapper.buildPaymentOrderDTO(paymentOrder));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping("/paymentorders/{internalId}")
  public ResponseEntity<String> createOrders(
      @RequestBody List<PaymentOrderDTO> dtos, @PathVariable("internalId") String internalId) {
    try {
      List<PaymentOrder> paymentOrders = new ArrayList<>();
      for (PaymentOrderDTO dto : dtos) {
        paymentOrders.add(PaymentOrderMapper.buildPaymentOrder(dto));
      }
      this.service.createOrders(paymentOrders, internalId);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping("/createOrdersByCSV/{internalId}")
  public ResponseEntity<List<PaymentOrderDTO>> createOrdersByCSV(
      @RequestParam("file") MultipartFile file, @PathVariable("internalId") String internalId) {
    try {
      List<PaymentOrderDTO> paymentsDTO =
          this.service.createOrdersByCSV(file, internalId).stream()
              .map(payment -> PaymentOrderMapper.buildPaymentOrderDTO(payment))
              .collect(Collectors.toList());
      return ResponseEntity.ok(paymentsDTO);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping("/validateCSVfile")
  public ResponseEntity<PaymentOrderDTO> validateCSVFile(@RequestParam("file") MultipartFile file) {
    this.service.validateCSVFile(file);
    return ResponseEntity.ok().build();
  }

  @PutMapping(path = "/{id}")
  public ResponseEntity<PaymentOrderDTO> approve(@PathVariable("id") String id) {
    try {
      PaymentOrder paymentOrder = this.service.approveOrder(id);
      return ResponseEntity.ok(PaymentOrderMapper.buildPaymentOrderDTO(paymentOrder));
    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }

  @PutMapping(path = "delete/{id}")
  public ResponseEntity<PaymentOrderDTO> deleteOrder(@PathVariable("id") String id) {
    try {
      PaymentOrder paymentOrder = this.service.deleteOrder(id);
      return ResponseEntity.ok(PaymentOrderMapper.buildPaymentOrderDTO(paymentOrder));
    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }

  @GetMapping(path = "/{paymentId}")
  public ResponseEntity<List<PaymentOrderDTO>> obtainByPaymentId(@PathVariable() String paymentId) {
    try {
      List<PaymentOrderDTO> paymentsDTO =
          this.service.obtainOrdersByPaymentId(paymentId).stream()
              .map(payment -> PaymentOrderMapper.buildPaymentOrderDTO(payment))
              .collect(Collectors.toList());
      return ResponseEntity.ok(paymentsDTO);

    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }

  @GetMapping
  public ResponseEntity<List<PaymentOrderDTO>> obtainOrdersByProperties(
      @RequestParam String groupId,
      @RequestParam(required = false) String id,
      @RequestParam(required = false) String state,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate) {
    try {
      List<PaymentOrderDTO> paymentsDTO =
          this.service.obtainOrdersByFilter(id, state, fromDate, endDate, groupId).stream()
              .map(payment -> PaymentOrderMapper.buildPaymentOrderDTO(payment))
              .collect(Collectors.toList());
      return ResponseEntity.ok(paymentsDTO);

    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }
}
