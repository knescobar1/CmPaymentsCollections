package com.banquito.cmpaymentscollections.payments.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PAYMENT_REQUIRED)
public class NotEnoughtFundsException extends RuntimeException {
  public NotEnoughtFundsException(String message) {
    super(message);
  }
}
