package com.banquito.cmpaymentscollections.payments.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class EmailException extends RuntimeException {
  public EmailException(String message) {
    super(message);
  }
}
