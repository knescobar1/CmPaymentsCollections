package com.banquito.cmpaymentscollections.payments.service;

import com.banquito.cmpaymentscollections.payments.dao.PaymentOrderRepository;
import com.banquito.cmpaymentscollections.payments.dao.PaymentRepository;
import com.banquito.cmpaymentscollections.payments.enums.StateEnum;
import com.banquito.cmpaymentscollections.payments.enums.StateOrderEnum;
import com.banquito.cmpaymentscollections.payments.exception.CSVFileException;
import com.banquito.cmpaymentscollections.payments.exception.PaymentServiceException;
import com.banquito.cmpaymentscollections.payments.model.Payment;
import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import java.io.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentOrderService {
  private final PaymentRepository paymentRepository;
  private final PaymentOrderRepository paymentOrderRepository;

  public PaymentOrder create(PaymentOrder paymentOrder, String internalId) {
    paymentOrder.setInternalId(UUID.randomUUID().toString());
    paymentOrder.setState(StateEnum.PENDING.getValue());
    Optional<Payment> paymentOpt =
        this.paymentRepository.findByInternalId(paymentOrder.getPaymentId());
    if (!paymentOpt.isPresent()) {
      throw new PaymentServiceException("No se encontro el pago con id " + internalId);
    }
    Payment paymentDB = paymentOpt.get();
    paymentDB.setRecords(paymentDB.getRecords() + 1);
    paymentDB.setTotalPaymentValue(paymentDB.getTotalPaymentValue().add(paymentOrder.getAmount()));
    this.paymentRepository.save(paymentDB);
    return this.paymentOrderRepository.save(paymentOrder);
  }

  public void createOrders(List<PaymentOrder> orders, String internalId) {
    for (PaymentOrder paymentOrderItem : orders) {
      log.info(paymentOrderItem.toString());
      this.create(paymentOrderItem, internalId);
    }
  }

  public PaymentOrder deleteOrder(String internalId) {
    PaymentOrder paymentOrderDb = this.obtainById(internalId);
    paymentOrderDb.setPaymentId(StateOrderEnum.DELETED.getValue());
    this.paymentOrderRepository.save(paymentOrderDb);
    return paymentOrderDb;
  }

  public PaymentOrder obtainById(String id) {
    Optional<PaymentOrder> paymentOrderOpt = this.paymentOrderRepository.findByInternalId(id);
    if (paymentOrderOpt.isPresent()) {
      return paymentOrderOpt.get();
    } else {
      return null;
    }
  }

  public PaymentOrder update(PaymentOrder paymentOrder) {
    PaymentOrder paymentOrderDb = this.obtainById(paymentOrder.getId());
    paymentOrderDb.setCreditorName(paymentOrder.getCreditorName());
    paymentOrderDb.setCreditorAccount(paymentOrder.getCreditorAccount());
    paymentOrderDb.setCreditorAccountType(paymentOrder.getCreditorAccountType());
    paymentOrderDb.setCreditorEmail(paymentOrder.getCreditorEmail());
    paymentOrderDb.setAmount(paymentOrder.getAmount());
    paymentOrderDb.setDescription(paymentOrder.getDescription());
    this.paymentOrderRepository.save(paymentOrderDb);
    return paymentOrderDb;
  }

  public void validateCSVFile(MultipartFile file) throws CSVFileException {
    List<String> headers = new ArrayList<>();
    headers.add("CONTRAPARTIDA CODIGO DE BENEFICARIO Y/O EMPLEADO");
    headers.add("NUMERO DE DOCUMENTO DE BENEFICARIO Y/O EMPLEADO");
    headers.add("TIPO DE DOCUMENTO DE BENEFICARIO Y/O EMPLEADO");
    headers.add("NOMBRES DE BENEFICARIO Y/O EMPLEADO");
    headers.add("NUMERO DE CUENTA");
    headers.add("TIPO DE CUENTA");
    headers.add("EMAIL DE BENEFICARIO Y/O EMPLEADO");
    headers.add("VALOR A PAGAR");
    headers.add("REFERENCIA");
    try {
      CsvParserSettings settings = new CsvParserSettings();
      InputStream inputStream = file.getInputStream();
      settings.setHeaderExtractionEnabled(true);
      CsvParser parser = new CsvParser(settings);
      List<Record> parseAllRecords = parser.parseAllRecords(inputStream);
      for (String header : headers) {
        try {
          parseAllRecords.get(0).getString(header);
        } catch (Exception e) {
          log.error("No existe el encabezado {} en el archivo.", header);
          throw new CSVFileException("No existe el encabezado " + header + " en el archivo.");
        }
      }
      for (int i = 0; i < parseAllRecords.size(); i++) {
        for (int j = 0; j < headers.size(); j++) {
          if (validateColumn(parseAllRecords.get(i), headers.get(j))) {
            log.error("El dato {} en la columna {} es obligatorio", headers.get(j), (i + 1));
            throw new CSVFileException(
                "El dato " + headers.get(j) + " en la columna " + (i + 1) + " es obligatorio");
          }
        }
      }
    } catch (IOException e) {
      log.error(
          "El archivo no se encuentra en el formato correcto. La separación debe estar dada por"
              + " comas (,)");
      throw new CSVFileException(
          "El archivo no se encuentra en el formato correcto. La separación debe estar dada por"
              + " comas (,)");
    }
  }

  public List<PaymentOrder> createOrdersByCSV(MultipartFile file, String internalId)
      throws IOException, PaymentServiceException {
    Optional<Payment> paymentOpt = this.paymentRepository.findByInternalId(internalId);
    if (!paymentOpt.isPresent()) {
      throw new PaymentServiceException("No se encontro el pago con id " + internalId);
    }

    Payment paymentDB = paymentOpt.get();
    List<PaymentOrder> orders = new ArrayList<>();
    CsvParserSettings settings = new CsvParserSettings();
    InputStream inputStream = file.getInputStream();
    settings.setHeaderExtractionEnabled(true);
    CsvParser parser = new CsvParser(settings);
    List<Record> parseAllRecords = parser.parseAllRecords(inputStream);
    BigDecimal paymentValue = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
    for (Record record : parseAllRecords) {
      PaymentOrder paymentsOrder =
          PaymentOrder.builder()
              .internalId(UUID.randomUUID().toString())
              .referenceId(record.getString("CONTRAPARTIDA CODIGO DE BENEFICARIO Y/O EMPLEADO"))
              .paymentId(paymentDB.getInternalId())
              .creditorAccount(record.getBigInteger("NUMERO DE CUENTA"))
              .creditorAccountType(record.getString("TIPO DE CUENTA"))
              .creditorIdentification(
                  record.getString("NUMERO DE DOCUMENTO DE BENEFICARIO Y/O EMPLEADO"))
              .creditorIdentificationType(
                  record.getString("TIPO DE DOCUMENTO DE BENEFICARIO Y/O EMPLEADO"))
              .creditorName(record.getString("NOMBRES DE BENEFICARIO Y/O EMPLEADO"))
              .creditorEmail(record.getString("EMAIL DE BENEFICARIO Y/O EMPLEADO "))
              .amount(record.getBigDecimal("VALOR A PAGAR"))
              .state(StateEnum.PENDING.getValue())
              .description(record.getString("REFERENCIA"))
              .build();
      paymentValue = paymentValue.add(paymentsOrder.getAmount());
      orders.add(paymentsOrder);
    }
    paymentDB.setTotalPaymentValue(paymentValue);
    paymentDB.setRecords(parseAllRecords.size());
    this.paymentRepository.save(paymentDB);
    this.paymentOrderRepository.saveAll(orders);
    return orders;
  }

  public PaymentOrder approveOrder(String orderId) {
    PaymentOrder paymentOrderDb = this.obtainById(orderId);
    paymentOrderDb.setState(StateEnum.APPROVED.getValue());
    return this.paymentOrderRepository.save(paymentOrderDb);
  }

  public List<PaymentOrder> obtainOrdersByReferenceId(String referenceId) {
    return this.paymentOrderRepository.findByReferenceIdOrderByPaymentDate(referenceId);
  }

  public List<PaymentOrder> obtainOrdersByPaymentId(String paymentId) {
    return this.paymentOrderRepository.findByPaymentId(paymentId);
  }

  public List<PaymentOrder> obtainOrdersByCreditorIdentification(String creditorIdentification) {
    return this.paymentOrderRepository.findByCreditorIdentificationOrderByPaymentDate(
        creditorIdentification);
  }

  public List<PaymentOrder> obtainOrdersByAmountGreaterThan(BigDecimal amount) {
    return this.paymentOrderRepository.findByAmountGreaterThanOrderByPaymentDate(amount);
  }

  public List<PaymentOrder> obtainOrdersByFilter(
      String id, String state, Date fromDate, Date endDate, String groupId) {
    List<PaymentOrder> orders = new ArrayList<PaymentOrder>();
    List<Payment> payments =
        this.paymentRepository.findByGroupInternalIdOrderByCreationDate(groupId);
    for (Payment payment : payments) {
      orders.addAll(
          this.paymentOrderRepository.findOrdersByProperties(
              id, state, fromDate, endDate, payment.getInternalId()));
    }
    return orders;
  }

  private Boolean validateColumn(Record column, String header) {
    Boolean empty = false;
    try {
      if (column.getString(header).isEmpty()) {
        empty = true;
      }
      return empty;
    } catch (Exception e) {
      return empty = true;
    }
  }
}
