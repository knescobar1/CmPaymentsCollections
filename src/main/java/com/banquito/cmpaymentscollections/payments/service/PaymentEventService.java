package com.banquito.cmpaymentscollections.payments.service;

import com.banquito.core.account.dto.TransactionDTO;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class PaymentEventService {

  private final PaymentService paymentService;

  @KafkaListener(topics = "transaction_pato", groupId = "foo2", containerFactory = "fooListener")
  public void listenOrdersUpdate(List<TransactionDTO> transactionDTOs) {
    log.info("Received TransactionDTOs: {}", transactionDTOs);
    paymentService.updateOrders(transactionDTOs);

    log.info("Completed updating transactions");
  }
}
