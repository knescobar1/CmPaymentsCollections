package com.banquito.cmpaymentscollections.payments.service;

import com.banquito.cmpaymentscollections.config.BaseURLValues;
import com.banquito.cmpaymentscollections.payments.dao.PaymentOrderRepository;
import com.banquito.cmpaymentscollections.payments.dao.PaymentRepository;
import com.banquito.cmpaymentscollections.payments.dto.AccountDTO;
import com.banquito.cmpaymentscollections.payments.dto.DepositDTO;
import com.banquito.cmpaymentscollections.payments.dto.JournalDTO;
import com.banquito.cmpaymentscollections.payments.dto.WithdrawalDTO;
import com.banquito.cmpaymentscollections.payments.enums.StateEnum;
import com.banquito.cmpaymentscollections.payments.enums.StateOrderEnum;
import com.banquito.cmpaymentscollections.payments.exception.EmailException;
import com.banquito.cmpaymentscollections.payments.exception.NotEnoughtFundsException;
import com.banquito.cmpaymentscollections.payments.exception.PaymentServiceException;
import com.banquito.cmpaymentscollections.payments.model.Payment;
import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import com.banquito.core.account.dto.TransactionDTO;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.*;
import javax.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentService {
  private final PaymentRepository paymentRepository;

  private final KafkaTemplate<String, Object> kafkaTemplate;
  private final PaymentOrderRepository paymentOrderRepository;
  private final RestTemplate restTemplate;
  private final BaseURLValues baseURLs;
  private final SequenceService sequenceService;
  private final EmailService emailService;
  private String mail;

  public Payment create(Payment payment) {
    payment.setProcessDate(payment.getProcessDate());
    payment.setState(StateEnum.PENDING.getValue());
    payment.setCreationDate(new Date());
    payment.setInternalId(UUID.randomUUID().toString());
    this.paymentRepository.save(payment);
    log.info("Pago creado {}", payment);
    return payment;
  }

  public Payment obtainByInternalId(String id) {
    Optional<Payment> paymentOpt = this.paymentRepository.findByInternalId(id);
    if (paymentOpt.isPresent()) {
      return paymentOpt.get();
    } else {
      return null;
    }
  }

  public Payment update(Payment payment) {
    Payment paymentDb = this.obtainByInternalId(payment.getId());
    paymentDb.setLastModifiedDate(new Date());
    paymentDb.setProcessDate(payment.getProcessDate());
    return this.paymentRepository.save(paymentDb);
  }

  public List<Payment> obtainPaymentsByFilter(
      String groupId,
      String id,
      String serviceName,
      String state,
      Date fromDate,
      Date endDate,
      String description) {
    return this.paymentRepository.findPaymentsByProperties(
        groupId, id, serviceName, state, fromDate, endDate, description);
  }

  public List<Payment> obtainPaymentsByState(
      String serviceName, String state, Date fromDate, Date endDate, String groupId) {
    return this.paymentRepository.findPaymentsByPropertiesState(
        serviceName, state, fromDate, endDate, groupId);
  }

  @Async
  public Payment approvePayment(String id, String mail)
      throws NotEnoughtFundsException, PaymentServiceException, ParseException {
    this.mail = mail;
    Payment paymentDb = this.obtainByInternalId(id);
    AccountDTO debtorAccount =
        this.restTemplate.getForObject(
            baseURLs.getCoreAccountsTransactionsURL()
                + "/accounts/accountNumber/"
                + paymentDb.getDebtorAccount(),
            AccountDTO.class);
    if (paymentDb.getTotalPaymentValue().compareTo(debtorAccount.getBalance()) == 1) {
      log.error(
          "La cuenta {} no tiene fondos suficientes para realizar los pagos.",
          paymentDb.getDebtorAccount());
      throw new NotEnoughtFundsException(
          "La cuenta "
              + paymentDb.getDebtorAccount()
              + " no tiene fondos suficientes para realizar los pagos.");
    }
    paymentDb.setState(StateEnum.APPROVED.getValue());
    this.payOrders(paymentDb);
    return this.paymentRepository.save(paymentDb);
  }

  public void payOrders(Payment payment) throws PaymentServiceException {
    List<DepositDTO> depositDTOSList = new ArrayList<DepositDTO>();
    Boolean validation =
        this.restTemplate.getForObject(
            baseURLs.getCmAccountingGeneralLedgerAccountURL()
                + "/ledgerAccount/Validation/typeTransaction=PAYMENTS",
            Boolean.class);
    if (!validation) {
      throw new PaymentServiceException("Ocurrio un error");
    }
    log.info("Pago: {}", payment.toString());
    WithdrawalDTO withdrawal =
        WithdrawalDTO.builder()
            .accountNumber(payment.getDebtorAccount())
            .amount(payment.getTotalPaymentValue())
            .notes(payment.getServiceDescription())
            .transactionChannel("CM PAGOS BANQUITO")
            .documentNumber(this.sequenceService.getNextDN())
            .transactionNumber(this.sequenceService.getNextTN())
            .build();

    WithdrawalDTO initialWithdrawal =
        this.restTemplate.postForObject(
            baseURLs.getCoreAccountsTransactionsURL() + "/transaction/withdrawal",
            withdrawal,
            WithdrawalDTO.class);
    log.info("El retiro de la cuenta de debito es: {}", initialWithdrawal.toString());
    List<PaymentOrder> paymentOrders =
        this.paymentOrderRepository.findByPaymentIdAndState(
            payment.getInternalId(), StateEnum.PENDING.getValue());
    log.info("Ordenes de pago");
    for (PaymentOrder paymentOrder : paymentOrders) {
      DepositDTO depositDTO =
          DepositDTO.builder()
              .referenceId(payment.getInternalId())
              .accountNumber(paymentOrder.getCreditorAccount())
              .amount(paymentOrder.getAmount())
              .notes(paymentOrder.getInternalId())
              .transactionChannel("CM PAGOS BANQUITO")
              .documentNumber(this.sequenceService.getNextDN())
              .transactionNumber(this.sequenceService.getNextTN())
              .build();
      log.info("Deposito creado: {}", depositDTO);
      depositDTOSList.add(depositDTO);
    }

    this.kafkaTemplate.send("payments_pato", depositDTOSList);
  }

  public void updateOrders(List<TransactionDTO> transactionDTOS) {
    List<PaymentOrder> paymentOrdersFailed = new ArrayList<>();
    List<PaymentOrder> paymentOrders = new ArrayList<PaymentOrder>();
    List<String> internalIds = new ArrayList<>();
    BigDecimal paidValue = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
    Integer paidRecords = 0;

    for (TransactionDTO transactionDTO : transactionDTOS) {
      internalIds.add(transactionDTO.getReference());
      paidValue = paidValue.add(transactionDTO.getAmount());
      paidRecords++;
    }
    log.info("SIZE TRANSACCIONES: {}: ,IDS: {}" + transactionDTOS.size(), internalIds.toString());
    paymentOrders = this.paymentOrderRepository.findByInternalIdIn(internalIds);
    Payment payment = this.obtainByInternalId(paymentOrders.get(0).getPaymentId());
    log.info("Pago procesandose: {}", payment.toString());

    JournalDTO journalPaymentOrdersDTO =
        JournalDTO.builder()
            .journalId("")
            .amount(paidValue)
            .transactionReference(payment.getInternalId())
            .transactionDate(new Date())
            .type("PAYMENTS")
            .description(payment.getServiceDescription())
            .build();
    log.info("Asiento creado para valor de las órdenes de pago : {}", journalPaymentOrdersDTO);
    HttpEntity<JournalDTO> journalTransactionPaymentOrders =
        new HttpEntity<>(journalPaymentOrdersDTO);
    JournalDTO journal =
        this.restTemplate.postForObject(
            baseURLs.getCmAccountingJournalEntryURL(),
            journalTransactionPaymentOrders,
            JournalDTO.class);
    for (PaymentOrder paymentOrder : paymentOrders) {
      paymentOrder.setPaymentDate(new Date());
      paymentOrder.setState(StateOrderEnum.DONE.getValue());
      log.info("Orden procesada {}", paymentOrder);
    }
    this.paymentOrderRepository.saveAll(paymentOrders);
    paymentOrdersFailed =
        this.paymentOrderRepository.findByPaymentIdAndState(
            payment.getInternalId(), StateEnum.PENDING.getValue());
    for (PaymentOrder paymentOrder : paymentOrdersFailed) {
      paymentOrder.setState(StateOrderEnum.FAILED.getValue());
      log.info("Orden fallida {}", paymentOrder);
    }
    this.paymentOrderRepository.saveAll(paymentOrdersFailed);
    if (payment.getTotalPaymentValue().compareTo(paidValue) != 0) {
      DepositDTO returnDepositDTO =
          DepositDTO.builder()
              .referenceId(payment.getInternalId())
              .accountNumber(payment.getDebtorAccount())
              .amount(payment.getTotalPaymentValue().subtract(paidValue))
              .notes("(REGRESO RETIRO INICIAL) " + payment.getServiceDescription())
              .transactionChannel("CM PAGOS BANQUITO")
              .documentNumber(this.sequenceService.getNextDN())
              .transactionNumber(this.sequenceService.getNextTN())
              .build();
      log.info(
          "Deposito creado para diferencia de retiro inicial: {}", returnDepositDTO.toString());
      HttpEntity<DepositDTO> requestDeposit = new HttpEntity<>(returnDepositDTO);
      this.restTemplate.postForObject(
          baseURLs.getCoreAccountsTransactionsURL() + "/transaction/deposit",
          requestDeposit,
          TransactionDTO.class);
      JournalDTO journalReturnDepositDTO =
          JournalDTO.builder()
              .journalId("")
              .amount(returnDepositDTO.getAmount())
              .transactionReference(returnDepositDTO.getTransactionNumber())
              .transactionDate(new Date())
              .type("PAYMENTS")
              .description(returnDepositDTO.getNotes())
              .build();
      log.info(
          "Asiento creado para diferencia de retiro inicial: {}",
          journalReturnDepositDTO.toString());
      HttpEntity<JournalDTO> journalReturnDepositTransaction =
          new HttpEntity<>(journalReturnDepositDTO);
      this.restTemplate.postForObject(
          baseURLs.getCmAccountingJournalEntryURL(),
          journalReturnDepositTransaction,
          JournalDTO.class);
    }
    payment.setPaidRecords(paidRecords);
    payment.setFailedRecords(payment.getRecords() - paidRecords);
    payment.setPaidValue(paidValue);
    payment.setFailedValue(payment.getTotalPaymentValue().subtract(paidValue));
    payment.setJournalId(journal.getJournalId());
    log.info("Pagos creados {}", payment);
    this.paymentRepository.save(payment);
    log.info(
        "Resumen de pago, items pagados: {} , Valor pagado: {} , Items fallidos: {} , Valor"
            + " fallido: {}",
        paidRecords,
        paidValue,
        payment.getRecords() - paidRecords,
        payment.getTotalPaymentValue().subtract(paidValue));
    try {
      log.info("Enviando correo con el resumen de los pagos a {}.", mail);
      this.emailService.sendPaymentsSummary(
          mail,
          payment.getServiceDescription(),
          paidRecords,
          paidValue,
          payment.getRecords() - paidRecords,
          payment.getTotalPaymentValue().subtract(paidValue));
    } catch (MessagingException e) {
      log.error("Error al enviar el correo a {}.", mail, e);
      throw new EmailException("Lo sentimos, no hemos podido enviar el correo.");
    }
  }
}
