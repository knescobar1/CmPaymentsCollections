package com.banquito.cmpaymentscollections.payments.service;

import com.banquito.cmpaymentscollections.payments.config.EmailConfig;
import java.math.BigDecimal;
import java.util.Objects;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailService {
  private final JavaMailSender emailSender;
  private final SimpleMailMessage template;
  private final EmailConfig emailConfig;

  @Async
  public void sendPaymentsSummary(
      String userEmail,
      String description,
      Integer paidRecords,
      BigDecimal paidValue,
      Integer failedRecords,
      BigDecimal failedValue)
      throws MessagingException {
    MimeMessage mimeMessage = emailSender.createMimeMessage();
    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "utf-8");

    String emailBody =
        String.format(
            Objects.requireNonNull(template.getText()),
            description,
            paidRecords,
            paidValue,
            failedRecords,
            failedValue);

    message.setFrom(this.emailConfig.getEmailAddress());
    message.setTo(userEmail);
    message.setSubject("Resumen de pago realizado " + description);
    message.setText(emailBody, true);

    emailSender.send(mimeMessage);
  }
}
