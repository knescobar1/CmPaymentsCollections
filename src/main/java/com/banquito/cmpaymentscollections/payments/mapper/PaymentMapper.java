package com.banquito.cmpaymentscollections.payments.mapper;

import com.banquito.cmpaymentscollections.payments.dto.PaymentDTO;
import com.banquito.cmpaymentscollections.payments.model.Payment;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaymentMapper {
  public static Payment buildPayment(PaymentDTO dto) {
    return Payment.builder()
            .internalId(dto.getInternalId())
            .groupInternalId(dto.getGroupInternalId())
            .serviceName(dto.getServiceName())
            .serviceDescription(dto.getServiceDescription())
            .state(dto.getState())
            .debtorAccount(dto.getDebtorAccount())
            .processDate(dto.getProcessDate())
            .records(dto.getRecords())
            .paidRecords(dto.getPaidRecords())
            .failedRecords(dto.getFailedRecords())
            .totalPaymentValue(dto.getTotalPaymentValue())
            .paidValue(dto.getPaidValue())
            .failedValue(dto.getFailedValue())
            .journalId(dto.getJournalId())
            .build();
  }

  public static PaymentDTO buildPaymentDTO(Payment payment) {
    return PaymentDTO.builder()
            .internalId(payment.getInternalId())
            .groupInternalId(payment.getGroupInternalId())
            .serviceName(payment.getServiceName())
            .serviceDescription(payment.getServiceDescription())
            .state(payment.getState())
            .debtorAccount(payment.getDebtorAccount())
            .processDate(payment.getProcessDate())
            .records(payment.getRecords())
            .paidRecords(payment.getPaidRecords())
            .failedRecords(payment.getFailedRecords())
            .totalPaymentValue(payment.getTotalPaymentValue())
            .paidValue(payment.getPaidValue())
            .failedValue(payment.getFailedValue())
            .journalId(payment.getJournalId())
            .build();
  }
}
