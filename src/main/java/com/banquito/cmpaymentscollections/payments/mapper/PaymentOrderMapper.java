package com.banquito.cmpaymentscollections.payments.mapper;

import com.banquito.cmpaymentscollections.payments.dto.PaymentOrderDTO;
import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaymentOrderMapper {

  public static PaymentOrder buildPaymentOrder(PaymentOrderDTO dto) {
    return PaymentOrder.builder()
            .internalId(dto.getInternalId())
            .referenceId(dto.getReferenceId())
            .paymentId(dto.getPaymentId())
            .creditorIdentification(dto.getCreditorIdentification())
            .creditorIdentificationType(dto.getCreditorIdentificationType())
            .creditorName(dto.getCreditorName())
            .creditorAccount(dto.getCreditorAccount())
            .creditorAccountType(dto.getCreditorAccountType())
            .creditorEmail(dto.getCreditorEmail())
            .amount(dto.getAmount())
            .state(dto.getState())
            .paymentDate(dto.getPaymentDate())
            .description(dto.getDescription())
            .build();
  }

  public static PaymentOrderDTO buildPaymentOrderDTO(PaymentOrder paymentOrder) {
    return PaymentOrderDTO.builder()
            .referenceId(paymentOrder.getReferenceId())
            .internalId(paymentOrder.getInternalId())
            .paymentId(paymentOrder.getPaymentId())
            .creditorIdentification(paymentOrder.getCreditorIdentification())
            .creditorIdentificationType(paymentOrder.getCreditorIdentificationType())
            .creditorName(paymentOrder.getCreditorName())
            .creditorAccount(paymentOrder.getCreditorAccount())
            .creditorAccountType(paymentOrder.getCreditorAccountType())
            .creditorEmail(paymentOrder.getCreditorEmail())
            .amount(paymentOrder.getAmount())
            .state(paymentOrder.getState())
            .paymentDate(paymentOrder.getPaymentDate())
            .description(paymentOrder.getDescription())
            .build();
  }
}
