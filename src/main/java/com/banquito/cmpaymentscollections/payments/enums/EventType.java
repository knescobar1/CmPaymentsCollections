package com.banquito.cmpaymentscollections.payments.enums;

public enum EventType {
    CREATED, UPDATED, DELETED
}
