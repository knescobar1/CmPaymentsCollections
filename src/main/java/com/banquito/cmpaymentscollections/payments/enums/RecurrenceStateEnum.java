package com.banquito.cmpaymentscollections.payments.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum RecurrenceStateEnum {
    INACTIVE("INA", "Inactive"),
    ACTIVE("ACT", "Active");

    private final String value;
    private final String text;
}
