package com.banquito.cmpaymentscollections.payments.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum StateOrderEnum {
  DONE("DON", "Done"),
  FAILED("FAI", "Failed"),

  DELETED("DEL", "FAILED");

  private final String value;
  private final String text;
}
