package com.banquito.cmpaymentscollections.payments.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum StateEnum {
    PENDING("PEN", "Pending"),
    APPROVED("APP", "Approved");

    private final String value;
    private final String text;
}
