package com.banquito.cmpaymentscollections.payments.dao;

import com.banquito.cmpaymentscollections.payments.model.Payment;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PaymentRepository
    extends MongoRepository<Payment, String>, PaymentCustomRepository {

  Optional<Payment> findByInternalId(String internalId);

  List<Payment> findByGroupInternalIdOrderByCreationDate(String groupInternalId);

  List<Payment> findByStateAndProcessDateOrderByCreationDate(String state, Date processDate);
}
