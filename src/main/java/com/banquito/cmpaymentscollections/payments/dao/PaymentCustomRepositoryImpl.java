package com.banquito.cmpaymentscollections.payments.dao;

import com.banquito.cmpaymentscollections.payments.model.Payment;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class PaymentCustomRepositoryImpl implements PaymentCustomRepository {

  private final MongoTemplate template;

  public List<Payment> findPaymentsByProperties(
      String groupId,
      String id,
      String serviceName,
      String state,
      Date fromDate,
      Date endDate,
      String description) {
    final Query query = new Query();
    final List<Criteria> criteria = new ArrayList<>();
    criteria.add(Criteria.where("groupInternalId").is(groupId));
    if (id != null && !id.isEmpty()) criteria.add(Criteria.where("internalId").is(id));
    if (serviceName != null && !serviceName.isEmpty())
      criteria.add(Criteria.where("serviceName").is(serviceName));
    if (state != null && !state.isEmpty()) criteria.add(Criteria.where("state").is(state));
    if (fromDate != null && endDate != null)
      criteria.add(
          Criteria.where("processDate")
              .gte(fromDate)
              .andOperator(Criteria.where("processDate").lte(endDate)));
    if (fromDate != null && endDate == null)
      criteria.add(Criteria.where("processDate").gte(fromDate));
    if (fromDate == null && endDate != null)
      criteria.add(Criteria.where("processDate").lte(endDate));
    if (description != null && !description.isEmpty())
      criteria.add(Criteria.where("serviceDescription").regex(description));

    if (!criteria.isEmpty())
      query
          .addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])))
          .with(Sort.by(Sort.Direction.DESC, "creationDate"));
    return template.find(query, Payment.class);
  }

  public List<Payment> findPaymentsByPropertiesState(
      String serviceName, String state, Date fromDate, Date endDate, String groupId) {
    final Query query = new Query();
    final List<Criteria> criteria = new ArrayList<>();
    if (serviceName != null && !serviceName.isEmpty())
      criteria.add(Criteria.where("serviceName").is(serviceName));
    if (state != null && !state.isEmpty()) criteria.add(Criteria.where("state").is(state));
    if (fromDate != null && endDate != null)
      criteria.add(
          Criteria.where("processDate")
              .gte(fromDate)
              .andOperator(Criteria.where("processDate").lte(endDate)));
    if (fromDate != null && endDate == null)
      criteria.add(Criteria.where("processDate").gte(fromDate));
    if (fromDate == null && endDate != null)
      criteria.add(Criteria.where("processDate").lte(endDate));
    criteria.add(Criteria.where("groupInternalId").is(groupId));
    if (!criteria.isEmpty())
      query.addCriteria(
          new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
    return template.find(query, Payment.class);
  }
}
