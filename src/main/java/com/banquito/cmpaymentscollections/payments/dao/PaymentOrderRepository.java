package com.banquito.cmpaymentscollections.payments.dao;

import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PaymentOrderRepository
        extends MongoRepository<PaymentOrder, String>, PaymentOrderCustomRepository {

  List<PaymentOrder> findByReferenceIdOrderByPaymentDate(String counterpart);

  List<PaymentOrder> findByPaymentId(String paymentId);

  List<PaymentOrder> findByPaymentIdAndState(String paymentId, String state);

  List<PaymentOrder> findByCreditorIdentificationOrderByPaymentDate(String beneficiaryDocument);

  List<PaymentOrder> findByAmountGreaterThanOrderByPaymentDate(BigDecimal amount);

  Optional<PaymentOrder> findByInternalId(String internalId);

  List<PaymentOrder> findByInternalIdIn(List<String> internalIds);



}
