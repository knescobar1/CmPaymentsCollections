package com.banquito.cmpaymentscollections.payments.dao;

import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class PaymentOrderCustomRepositoryImpl implements PaymentOrderCustomRepository {

  private final MongoTemplate template;

  @Override
  public List<PaymentOrder> findOrdersByProperties(
      String id, String state, Date fromDate, Date endDate, String paymentId) {

    final Query query = new Query();
    final List<Criteria> criteria = new ArrayList<>();
    if (id != null && !id.isEmpty()) criteria.add(Criteria.where("internalId").is(id));
    if (paymentId != null && !paymentId.isEmpty())
      criteria.add(Criteria.where("paymentId").is(paymentId));
    if (state != null && !state.isEmpty()) criteria.add(Criteria.where("state").is(state));
    if (fromDate != null && endDate != null)
      criteria.add(Criteria.where("paymentDate").gte(fromDate).lte(endDate));
    if (fromDate != null && endDate == null)
      criteria.add(Criteria.where("paymentDate").gte(fromDate));
    if (fromDate == null && endDate != null)
      criteria.add(Criteria.where("paymentDate").lte(endDate));

    if (!criteria.isEmpty())
      query.addCriteria(
          new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
    return template.find(query, PaymentOrder.class);
  }
}
