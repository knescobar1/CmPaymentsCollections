package com.banquito.cmpaymentscollections.payments.dao;

import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import java.util.Date;
import java.util.List;

public interface PaymentOrderCustomRepository {

  List<PaymentOrder> findOrdersByProperties(
      String id, String state, Date fromDate, Date endDate, String paymentId);
}
