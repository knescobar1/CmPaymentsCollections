package com.banquito.cmpaymentscollections.payments.dao;

import com.banquito.cmpaymentscollections.payments.model.Payment;
import java.util.Date;
import java.util.List;

public interface PaymentCustomRepository {

  List<Payment> findPaymentsByProperties(
      String groupId,
      String id,
      String serviceName,
      String state,
      Date fromDate,
      Date endDate,
      String description);

  List<Payment> findPaymentsByPropertiesState(
      String serviceName, String state, Date fromDate, Date endDate, String groupId);
}
