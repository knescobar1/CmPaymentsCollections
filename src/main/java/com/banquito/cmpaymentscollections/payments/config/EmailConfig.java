package com.banquito.cmpaymentscollections.payments.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;

@Configuration
public class EmailConfig {
  @Getter private final String emailAddress;

  public EmailConfig(@Value("${spring.mail.username}") String emailAddress) {
    this.emailAddress = emailAddress;
  }

  @Bean
  public SimpleMailMessage userAndPasswordTemplateMailBody() {
    String messageBody =
        "<h2 style='text-align: center'>Resumen del pago %s</h2></br>"
            + "<p>Ordenes pagadas: %s</p>"
            + "<p>Valor procesado: %s</p>"
            + "<p>Ordenes fallidas: %s</p>"
            + "<p>Valor fallido: %s</p>";

    SimpleMailMessage mailMessage = new SimpleMailMessage();
    mailMessage.setText(messageBody);

    return mailMessage;
  }
}
