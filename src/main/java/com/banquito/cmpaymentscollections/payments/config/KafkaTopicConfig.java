package com.banquito.cmpaymentscollections.payments.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

public class KafkaTopicConfig {
    @Bean
    public NewTopic amigoscodeTopic() {
        return TopicBuilder.name("payments_pato").build();
    }
}
