package com.banquito.cmpaymentscollections.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class ApplicationValues {
  private final String mongoHost;
  private final String mongoDB;
  private final String mongoUsr;
  private final String mongoPwd;
  private final String mongoAut;

  private final String projectId;

  private final String bucketName;

  public ApplicationValues(
      @Value("${banquito.cmpaymentscollections.mongo.host}") String mongoHost,
      @Value("${banquito.cmpaymentscollections.mongo.db}") String mongoDB,
      @Value("${banquito.cmpaymentscollections.mongo.usr}") String mongoUsr,
      @Value("${banquito.cmpaymentscollections.mongo.pwd}") String mongoPwd,
      @Value("${banquito.cmpaymentscollections.mongo.aut}") String mongoAut,
      @Value("${banquito.cm.storage.project_id}") String projectId,
      @Value("${banquito.cm.storage.bucket_name}") String bucketName) {

    this.mongoHost = mongoHost;
    this.mongoDB = mongoDB;
    this.mongoUsr = mongoUsr;
    this.mongoPwd = mongoPwd;
    this.mongoAut = mongoAut;
    this.projectId = projectId;
    this.bucketName = bucketName;
  }
}
