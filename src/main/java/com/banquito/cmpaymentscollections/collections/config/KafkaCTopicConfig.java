package com.banquito.cmpaymentscollections.collections.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

public class KafkaCTopicConfig {
  @Bean
  public NewTopic banquitoCollTopic() {
    return TopicBuilder.name("collections_wesos").build();
  }
}
