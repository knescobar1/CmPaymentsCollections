package com.banquito.cmpaymentscollections.collections.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import com.banquito.cmpaymentscollections.collections.process.CollectPaymentTask;
import com.banquito.cmpaymentscollections.collections.service.CollectionOrderService;
import com.banquito.cmpaymentscollections.payments.service.SequenceService;

@Configuration
@EnableAutoConfiguration
@EnableBatchProcessing
public class JobConfig {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Autowired
    private CollectionOrderService collectionOrderService;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Bean
    protected Step collectPaymentTask() {
        return steps
                .get("collectPaymentTask")
                .tasklet(new CollectPaymentTask(collectionOrderService,sequenceService,kafkaTemplate))
                .build();
    }

    @Bean
    public Job processTransactionJob() {
        return jobs
                .get("processTransactionJob")
                .start(collectPaymentTask())
                .build();
    }
}
