package com.banquito.cmpaymentscollections.collections.enums;

public enum CollectionEnum {
  ACTIVE("ACT", "Active"),
  INACTIVE("INA", "Inactive"),
  PROCESS("PRO", "Process"),
  COLLECTED("COL", "Collected");

  private final String value;
  private final String text;

  private CollectionEnum(String value, String text) {
    this.value = value;
    this.text = text;
  }

  public String getText() {
    return this.text;
  }

  public String getValue() {
    return this.value;
  }
}
