package com.banquito.cmpaymentscollections.collections.enums;

public enum ProcessingStateEnum {
  CREATED("CRE", "Created"),
  PROCESSED("PRO", "Processed");

  private final String value;
  private final String text;

  private ProcessingStateEnum(String value, String text) {
    this.value = value;
    this.text = text;
  }

  public String getText() {
    return this.text;
  }

  public String getValue() {
    return this.value;
  }
}
