package com.banquito.cmpaymentscollections.collections.enums;

public enum EventType {
  CREATED,
  UPDATED,
  DELETED
}
