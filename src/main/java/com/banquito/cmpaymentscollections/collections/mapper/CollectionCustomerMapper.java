package com.banquito.cmpaymentscollections.collections.mapper;

import com.banquito.cmpaymentscollections.collections.dto.CollectionCustomerDTO;
import com.banquito.cmpaymentscollections.collections.model.CollectionCustomer;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CollectionCustomerMapper {

  public static CollectionCustomer buildCollectionCustomer(CollectionCustomerDTO dto) {
    return CollectionCustomer.builder()
        .customerId(dto.getCustomerId())
        .typeCustomerId(dto.getTypeCustomerId())
        .fullName(dto.getFullName())
        .build();
  }

  public static CollectionCustomerDTO buildCollectionCustomerDTO(
      CollectionCustomer collectionCustomer) {
    return CollectionCustomerDTO.builder()
        .customerId(collectionCustomer.getCustomerId())
        .typeCustomerId(collectionCustomer.getTypeCustomerId())
        .fullName(collectionCustomer.getFullName())
        .build();
  }
}
