package com.banquito.cmpaymentscollections.collections.mapper;

import com.banquito.cmpaymentscollections.collections.dto.CollectionDTO;
import com.banquito.cmpaymentscollections.collections.model.Collection;
import java.util.Date;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CollectionMapper {
  public static Collection buildCollection(CollectionDTO dto) {
    return Collection.builder()
        .groupInternalId(dto.getGroupInternalId())
        .internalId(dto.getInternalId())
        .creditorAccount(dto.getCreditorAccount())
        .serviceOffered(
            CollectionServiceOfferedMapper.buildCollectionServiceOffered(dto.getServiceOffered()))
        .creationDate(new Date())
        .periodicity(dto.getPeriodicity())
        .reference(dto.getReference())
        .channel(dto.getChannel())
        .startCollectionDate(dto.getStartCollectionDate())
        .endCollectionDate(dto.getEndCollectionDate())
        .state(dto.getState())
        .records(dto.getRecords())
        .paidRecords(dto.getPaidRecords())
        .failedRecords(dto.getFailedRecords())
        .totalCollectionValue(dto.getTotalCollectionValue())
        .failedValue(dto.getFailedValue())
        .build();
  }

  public static CollectionDTO buildCollectionDTO(Collection collection) {
    return CollectionDTO.builder()
        .groupInternalId(collection.getGroupInternalId())
        .internalId(collection.getInternalId())
        .creditorAccount(collection.getCreditorAccount())
        .serviceOffered(
            CollectionServiceOfferedMapper.buildCollectionServiceOfferedDTO(
                collection.getServiceOffered()))
        .creationDate(collection.getCreationDate())
        .periodicity(collection.getPeriodicity())
        .reference(collection.getReference())
        .channel(collection.getChannel())
        .startCollectionDate(collection.getStartCollectionDate())
        .endCollectionDate(collection.getEndCollectionDate())
        .state(collection.getState())
        .records(collection.getRecords())
        .paidRecords(collection.getPaidRecords())
        .failedRecords(collection.getFailedRecords())
        .totalCollectionValue(collection.getTotalCollectionValue())
        .failedValue(collection.getFailedValue())
        .build();
  }
}
