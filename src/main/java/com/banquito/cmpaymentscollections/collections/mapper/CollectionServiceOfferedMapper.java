package com.banquito.cmpaymentscollections.collections.mapper;

import com.banquito.cmpaymentscollections.collections.dto.CollectionServiceOfferedDTO;
import com.banquito.cmpaymentscollections.collections.model.CollectionServiceOffered;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CollectionServiceOfferedMapper {
  public static CollectionServiceOffered buildCollectionServiceOffered(
      CollectionServiceOfferedDTO dto) {
    return CollectionServiceOffered.builder()
        .name(dto.getName())
        .description(dto.getDescription())
        .build();
  }

  public static CollectionServiceOfferedDTO buildCollectionServiceOfferedDTO(
      CollectionServiceOffered collectionServiceOffered) {
    return CollectionServiceOfferedDTO.builder()
        .name(collectionServiceOffered.getName())
        .description(collectionServiceOffered.getDescription())
        .build();
  }
}
