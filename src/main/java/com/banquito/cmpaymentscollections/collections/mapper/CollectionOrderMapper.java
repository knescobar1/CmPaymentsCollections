package com.banquito.cmpaymentscollections.collections.mapper;

import com.banquito.cmpaymentscollections.collections.dto.CollectionOrderDTO;
import com.banquito.cmpaymentscollections.collections.model.CollectionOrder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CollectionOrderMapper {

  public static CollectionOrder buildCollectionOrder(CollectionOrderDTO dto) {
    return CollectionOrder.builder()
        .collectionId(dto.getCollectionId())
        .referenceId(dto.getReferenceId())
        .internalId(dto.getInternalId())
        .customer(CollectionCustomerMapper.buildCollectionCustomer(dto.getCustomer()))
        .debtorAccount(dto.getDebtorAccount())
        .paymentWay(dto.getPaymentWay())
        .amount(dto.getAmount())
        .paid(dto.getPaid())
        .pending(dto.getPending())
        .reference(dto.getReference())
        .collectedDate(dto.getCollectedDate())
        .startCollectionDate(dto.getStartCollectionDate())
        .processingState(dto.getProcessingState())
        .state(dto.getState())
        .endCollectionDate(dto.getEndCollectionDate())
        .journalId(dto.getJournalId())
        .build();
  }

  public static CollectionOrderDTO buildCollectionOrderDTO(CollectionOrder collectionOrder) {
    return CollectionOrderDTO.builder()
        .collectionId(collectionOrder.getCollectionId())
        .referenceId(collectionOrder.getReferenceId())
        .internalId(collectionOrder.getInternalId())
        .customer(
            CollectionCustomerMapper.buildCollectionCustomerDTO(collectionOrder.getCustomer()))
        .debtorAccount(collectionOrder.getDebtorAccount())
        .paymentWay(collectionOrder.getPaymentWay())
        .amount(collectionOrder.getAmount())
        .paid(collectionOrder.getPaid())
        .pending(collectionOrder.getPending())
        .reference(collectionOrder.getReference())
        .collectedDate(collectionOrder.getCollectedDate())
        .startCollectionDate(collectionOrder.getStartCollectionDate())
        .endCollectionDate(collectionOrder.getEndCollectionDate())
        .processingState(collectionOrder.getProcessingState())
        .state(collectionOrder.getState())
        .journalId(collectionOrder.getJournalId())
        .build();
  }
}
