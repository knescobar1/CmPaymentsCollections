package com.banquito.cmpaymentscollections.collections.resource;

import com.banquito.cmpaymentscollections.collections.dto.CollectionDTO;
import com.banquito.cmpaymentscollections.collections.mapper.CollectionMapper;
import com.banquito.cmpaymentscollections.collections.model.Collection;
import com.banquito.cmpaymentscollections.collections.service.CollectionService;
import java.util.ArrayList;
import java.util.List;
import javax.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/collection")
@RequiredArgsConstructor
public class CollectionResource {

  private final CollectionService service;

  @PostMapping
  public ResponseEntity<CollectionDTO> create(@RequestBody CollectionDTO dto) {
    try {
      Collection collection = this.service.create(CollectionMapper.buildCollection(dto));
      return ResponseEntity.ok(CollectionMapper.buildCollectionDTO(collection));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping("/{collectionId}")
  public ResponseEntity<CollectionDTO> update(
      @PathVariable String collectionId, @RequestBody CollectionDTO dto) {
    try {
      Collection collection =
          this.service.update(
              CollectionMapper.buildCollection(dto).getStartCollectionDate(),
              CollectionMapper.buildCollection(dto).getEndCollectionDate(),
              collectionId);
      return ResponseEntity.ok(CollectionMapper.buildCollectionDTO(collection));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping("/setstate")
  public ResponseEntity<CollectionDTO> setState(@RequestBody CollectionDTO dto) {
    try {
      Collection collection =
          this.service.setState(CollectionMapper.buildCollection(dto).getInternalId());
      return ResponseEntity.ok(CollectionMapper.buildCollectionDTO(collection));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping("/activeInactive/{groupInternalId}")
  public ResponseEntity<List<CollectionDTO>> findActiveAndInactive(
      @PathVariable String groupInternalId) {
    try {
      List<Collection> collections = this.service.findActiveAndInactive(groupInternalId);
      List<CollectionDTO> dtos = new ArrayList<>();
      for (Collection collection : collections) {
        dtos.add(CollectionMapper.buildCollectionDTO(collection));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping
  public ResponseEntity<List<CollectionDTO>> obtainAll() {
    try {
      List<Collection> collection = this.service.findAll();
      List<CollectionDTO> collectionDTO = new ArrayList<>();
      for (Collection c : collection) {
        collectionDTO.add(CollectionMapper.buildCollectionDTO(c));
      }
      return ResponseEntity.ok(collectionDTO);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/{groupInternalId}")
  public ResponseEntity<List<CollectionDTO>> obtainCollectionByGroupInternalId(
      @PathVariable("groupInternalId") String groupInternalId) {
    try {
      List<Collection> collection = this.service.findByGroupInternalId(groupInternalId);
      List<CollectionDTO> collectionDTO = new ArrayList<>();
      for (Collection c : collection) {
        collectionDTO.add(CollectionMapper.buildCollectionDTO(c));
      }
      return ResponseEntity.ok(collectionDTO);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/CollectionByPeriodicityAndGroupInternalId")
  public ResponseEntity<List<CollectionDTO>> obtainCollectionByPeriodicityAndGroupInternalId(
      @PathParam("periodicity") String periodicity,
      @PathParam("groupInternalId") String groupInternalId) {
    try {
      List<Collection> collection =
          this.service.findByPeriodicityAndGroupInternalId(periodicity, groupInternalId);
      List<CollectionDTO> collectionDTO = new ArrayList<>();
      for (Collection c : collection) {
        collectionDTO.add(CollectionMapper.buildCollectionDTO(c));
      }
      return ResponseEntity.ok(collectionDTO);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/CollectionByStateAndGroupInternalId")
  public ResponseEntity<List<CollectionDTO>> obtainCollectionByStateAndGroupInternalId(
      @PathParam("state") String state, @PathParam("groupInternalId") String groupInternalId) {
    try {
      List<Collection> collection =
          this.service.findByStateAndGroupInternalId(state, groupInternalId);
      List<CollectionDTO> collectionDTO = new ArrayList<>();
      for (Collection c : collection) {
        collectionDTO.add(CollectionMapper.buildCollectionDTO(c));
      }
      return ResponseEntity.ok(collectionDTO);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/CollectionByServiceName")
  public ResponseEntity<List<CollectionDTO>> obtainCollectionByServiceNameAndGroupInternalId(
      @PathParam("serviceName") String serviceName,
      @PathParam("groupInternalId") String groupInternalId) {
    try {
      List<Collection> collection =
          this.service.findByServiceOfferedNameAndGroupInternalId(serviceName, groupInternalId);
      List<CollectionDTO> collectionDTO = new ArrayList<>();
      for (Collection c : collection) {
        collectionDTO.add(CollectionMapper.buildCollectionDTO(c));
      }
      return ResponseEntity.ok(collectionDTO);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/services")
  public ResponseEntity<List<CollectionDTO>> obtainServices() {
    try {
      List<Collection> collection = this.service.findAllService();
      List<CollectionDTO> collectionDTO = new ArrayList<>();
      for (Collection c : collection) {
        collectionDTO.add(CollectionMapper.buildCollectionDTO(c));
      }
      return ResponseEntity.ok(collectionDTO);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }
}
