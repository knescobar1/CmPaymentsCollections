package com.banquito.cmpaymentscollections.collections.resource;

import com.banquito.cmpaymentscollections.collections.dto.CollectionOrderDTO;
import com.banquito.cmpaymentscollections.collections.enums.ProcessingStateEnum;
import com.banquito.cmpaymentscollections.collections.exceptions.BadRequestException;
import com.banquito.cmpaymentscollections.collections.mapper.CollectionOrderMapper;
import com.banquito.cmpaymentscollections.collections.model.CollectionOrder;
import com.banquito.cmpaymentscollections.collections.service.CollectionOrderService;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "/collectionorder")
@RequiredArgsConstructor
public class CollectionOrderResource {
  private final CollectionOrderService service;

  @PostMapping
  public ResponseEntity<String> create(@RequestBody List<CollectionOrderDTO> dtos) {
    try {
      List<CollectionOrder> collectionOrders = new ArrayList<>();
      for (CollectionOrderDTO dto : dtos) {
        collectionOrders.add(CollectionOrderMapper.buildCollectionOrder(dto));
      }
      this.service.createWithList(collectionOrders);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping("/readycollection")
  public ResponseEntity<String> readyCollection(@RequestBody List<String> internalIds) {
    try {
      this.service.readyCollection(internalIds);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping("/validateCSVfile")
  public ResponseEntity<CollectionOrderDTO> validateCSVFile(
      @RequestParam("file") MultipartFile file) {
    this.service.validateCSVFile(file);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/uploadToPayCsv/{collectionId}")
  public ResponseEntity<String> uploadToPayCSVFile(
      @RequestParam("file") MultipartFile file, @PathVariable("collectionId") String collectionId) {
    try {
      return ResponseEntity.ok(this.service.uploadToPayCsv(file, collectionId));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PostMapping("/createOne")
  public ResponseEntity<String> createOne(@RequestBody CollectionOrderDTO dto) {
    try {
      CollectionOrder collectionOrder = CollectionOrderMapper.buildCollectionOrder(dto);
      this.service.createOne(collectionOrder);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping("/updateCollectionOrder")
  public ResponseEntity<CollectionOrder> updateCollectionOrder(@RequestBody CollectionOrderDTO dto)
      throws BadRequestException {
    try {
      CollectionOrder collectionOrder =
          CollectionOrder.builder()
              .internalId(dto.getInternalId())
              .debtorAccount(dto.getDebtorAccount())
              .paymentWay(dto.getPaymentWay())
              .amount(dto.getAmount())
              .reference(dto.getReference())
              .build();
      return ResponseEntity.ok(this.service.updateCollectionOrder(collectionOrder));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    } catch (BadRequestException e) {
      throw new BadRequestException(e.getMessage());
    }
  }

  @PutMapping("/changePaymentWay/{internalId}")
  public ResponseEntity<CollectionOrder> changePaymentWay(@PathVariable String internalId) {
    try {
      this.service.changePaymentWay(internalId);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @DeleteMapping("/deleteCollectionOrder/{internalId}")
  public ResponseEntity<String> deleteCollectionOrder(@PathVariable String internalId)
      throws BadRequestException {
    try {
      this.service.deleteCollectionOrder(internalId);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    } catch (BadRequestException e) {
      throw new BadRequestException("No se pudo eliminar el registro");
    }
  }

  @GetMapping(path = "/collectionOrderByReferenceIdAndCollectionId")
  public ResponseEntity<List<CollectionOrderDTO>> collectionOrderByReferenceIdAndCollectionId(
      @PathParam("collectionId") String collectionId,
      @PathParam("referenceId") String referenceId) {
    try {
      List<CollectionOrder> collectionOrders =
          this.service.findByReferenceIdAndCollectionId(referenceId, collectionId);
      List<CollectionOrderDTO> collectionOrderDTOs = new ArrayList<>();
      for (CollectionOrder collectionOrder : collectionOrders) {
        collectionOrderDTOs.add(CollectionOrderMapper.buildCollectionOrderDTO(collectionOrder));
      }
      return ResponseEntity.ok(collectionOrderDTOs);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/report/{serviceName}/{state}/{dateFrom}/{dateTo}")
  public ResponseEntity<List<CollectionOrderDTO>> reportCollection(
      @PathVariable String serviceName,
      @PathVariable List<String> state,
      @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFrom,
      @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateTo) {
    try {
      List<CollectionOrder> collectionOrders =
          this.service.reportCollection(serviceName, state, dateFrom, dateTo);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder collectionOrder : collectionOrders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(collectionOrder));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/{collectionId}")
  public ResponseEntity<List<CollectionOrderDTO>> findByCollectionId(
      @PathVariable String collectionId) {
    try {
      List<CollectionOrder> collectionOrders = this.service.findByCollectionId(collectionId);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder collectionOrder : collectionOrders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(collectionOrder));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/collectionsPaymentWay/{account}/{paymentWay}")
  public ResponseEntity<List<CollectionOrderDTO>> findByCollectionPaymentWay(
      @PathVariable BigInteger account,
      @PathVariable String paymentWay) {
    try {
      List<CollectionOrder> collectionOrders = this.service.getCollectionOrderPaymentWay(account,paymentWay);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder collectionOrder : collectionOrders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(collectionOrder));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }
  
  @PutMapping(path = "/payCollectionOrder/{internalId}")
  public ResponseEntity<String> payCollectionOrder(@PathVariable String internalId) {
    try {
      this.service.payCollectionOrder(internalId);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping(path = "/payOrder")
  public ResponseEntity<String> payOrder(
      @RequestParam("referenceId") String referenceId,
      @RequestParam("collectionId") String collectionId) {
    try {
      this.service.pay(referenceId, collectionId);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/CollectionOrderByStateAndCollectionId")
  public ResponseEntity<List<CollectionOrderDTO>> findByStateAndCollectionId(
      @PathParam("state") String state, @PathParam("collectionId") String collectionId) {
    try {
      List<CollectionOrder> orders =
          this.service.findByStateAndCollectionIdOrderByCustomerName(state, collectionId);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder order : orders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(order));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/collectionOrderByDebtorAccount/{account}")
  public ResponseEntity<List<CollectionOrderDTO>> findByDebtorAccount(
      @PathVariable BigInteger account) {
    try {
      List<CollectionOrder> orders = this.service.findByDebtorAccount(account);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder order : orders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(order));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/CollectionOrderBetweenDateAndCollectionId")
  public ResponseEntity<List<CollectionOrderDTO>> findByDateAndCollectionId(
      @PathParam("dateFrom") Date dateFrom,
      @PathParam("dateTo") Date dateTo,
      @PathParam("collectionId") String collectionId) {
    try {
      List<CollectionOrder> collectionOrders =
          this.service.findBetweenCollectedDateAndCollectionId(dateFrom, dateTo, collectionId);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder collectionOrder : collectionOrders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(collectionOrder));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/CollectionOrderByCollectionServiceName")
  public ResponseEntity<List<CollectionOrderDTO>> findByCollectionServiceName(
      @RequestParam("serviceName") String serviceName,
      @RequestParam("referenceId") String referenceId) {
    try {
      List<CollectionOrder> orders =
          this.service.obtainOrderbyCollectionServiceName(serviceName, referenceId);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder order : orders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(order));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping(path = "/processOrder")
  public ResponseEntity<String> processOrderCollection(@RequestParam String collectionId) {
    try {
      String message = this.service.processCollectionOrder(collectionId);
      return ResponseEntity.ok(message);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/orderCustomer/{customerId}")
  public ResponseEntity<List<CollectionOrderDTO>> obtainCollectionByCustomer(
      @PathVariable String customerId) {
    try {
      List<CollectionOrder> collectionOrders =
          this.service.obtainByCustomerAndState(
              ProcessingStateEnum.PROCESSED.getValue(), customerId);
      List<CollectionOrderDTO> dtos = new ArrayList<>();
      for (CollectionOrder collectionOrder : collectionOrders) {
        dtos.add(CollectionOrderMapper.buildCollectionOrderDTO(collectionOrder));
      }
      return ResponseEntity.ok(dtos);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }
}
