package com.banquito.cmpaymentscollections.collections.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class CSVFileException extends RuntimeException {
  public CSVFileException(String message) {
    super(message);
  }
}
