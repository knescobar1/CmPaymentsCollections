package com.banquito.cmpaymentscollections.collections.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CollectionServiceException extends RuntimeException {
  public CollectionServiceException(String message) {
    super(message);
  }
}
