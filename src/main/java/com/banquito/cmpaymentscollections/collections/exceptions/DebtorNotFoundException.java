package com.banquito.cmpaymentscollections.collections.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(org.springframework.http.HttpStatus.NOT_FOUND)
public class DebtorNotFoundException extends RuntimeException {
  public DebtorNotFoundException(String message) {
    super(message);
  }
}
