package com.banquito.cmpaymentscollections.collections.exceptions;

public class NotFoundException extends Exception {
    public NotFoundException(String message) {
        super(message);
    }
}

