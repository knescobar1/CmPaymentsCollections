package com.banquito.cmpaymentscollections.collections.dao;

import com.banquito.cmpaymentscollections.collections.model.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CollectionRepository extends MongoRepository<Collection, String> {
  Optional<Collection> findByInternalId(String internalId);

  List<Collection> findByCollectionId(String collectionId);

  List<Collection> findByCollectionIdAndState(String collectionId, String state);

  List<Collection> findByGroupInternalId(String groupInternalId);

  List<Collection> findByPeriodicityAndGroupInternalId(String periodicity, String groupInternalId);

  List<Collection> findByStateAndGroupInternalId(String state, String groupInternalId);

  List<Collection> findByStateInAndGroupInternalId(List<String> states, String groupInternalId);

  List<Collection> findByServiceOfferedName(String name);

  List<Collection> findByServiceOfferedNameAndGroupInternalId(String name, String groupInternalId);
}
