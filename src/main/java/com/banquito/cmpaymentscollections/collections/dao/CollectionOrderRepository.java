package com.banquito.cmpaymentscollections.collections.dao;

import com.banquito.cmpaymentscollections.collections.model.CollectionOrder;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CollectionOrderRepository extends MongoRepository<CollectionOrder, String> {

  Optional<CollectionOrder> findByInternalId(String internalId);

  List<CollectionOrder> findByCollectionIdOrderByCustomerFullName(String collectionId);

  List<CollectionOrder> findByReferenceIdAndCollectionId(String referenceId, String collectionId);

  List<CollectionOrder> findByStateAndCollectionIdOrderByCustomerFullName(
      String state, String collectionId);

  List<CollectionOrder> findByCollectedDateBetweenAndCollectionIdOrderByCustomerFullName(
      Date startCollectedDate, Date endCollectedDate, String collectionId);

  List<CollectionOrder>
      findByCollectionIdInAndStateInAndStartCollectionDateBetweenAndProcessingState(
          List<String> ids, List<String> State, Date dateFrom, Date dateTo, String processingState);

  List<CollectionOrder> findByProcessingStateAndCustomerCustomerId(
      String processState, String customerId);

  List<CollectionOrder> findByDebtorAccountAndStateAndPaymentWay(BigInteger debtorAccount, String state, String paymentWay);

  List<CollectionOrder> findByStateAndPaymentWayAndStartCollectionDateGreaterThanAndEndCollectionDateLessThan(
      String state, String paymentWay, Date startCollectionDate, Date endCollectionDate);
  
  List<CollectionOrder> findByStateAndPaymentWay(String state, String paymentWay);

  List<CollectionOrder> findByDebtorAccountAndPaymentWay(BigInteger debtorAccount, String paymentWay);
}
