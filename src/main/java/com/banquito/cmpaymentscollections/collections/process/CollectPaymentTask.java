package com.banquito.cmpaymentscollections.collections.process;
import java.math.BigDecimal;
import java.util.List;
import java.math.RoundingMode;

import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.banquito.cmpaymentscollections.collections.model.Collection;
import com.banquito.cmpaymentscollections.collections.model.CollectionOrder;
import com.banquito.cmpaymentscollections.collections.service.CollectionOrderService;
import com.banquito.cmpaymentscollections.payments.service.SequenceService;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.kafka.core.KafkaTemplate;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class CollectPaymentTask implements Tasklet, StepExecutionListener {

    private List<CollectionOrder> collectionOrders;

    private final CollectionOrderService collectionOrderService;

    private final SequenceService sequenceService;

    private final  KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public void beforeStep(StepExecution arg0) {
        collectionOrders = collectionOrderService.getCollectionOrderRecurrementToPay();
    }

    @Override
    public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
        BigDecimal fullPaymentValue = new BigDecimal(0);
        log.info("Cantidad de datos a pagar {}", collectionOrders.size() );
        for (CollectionOrder collectionOrder : collectionOrders) {  
            if (collectionOrder.getPending().compareTo(new BigDecimal(0)) == 0) {
                fullPaymentValue = collectionOrder.getAmount();
            } else {
                fullPaymentValue = collectionOrder.getPending();
            }
            Collection collectionDb = this.collectionOrderService.findCollectionById(collectionOrder.getCollectionId());
            
            TransactionDTO transactionDTO =
                TransactionDTO.builder()
                    .debtorAccountNumber(collectionOrder.getDebtorAccount())
                    .creditorAccountNumber(collectionDb.getCreditorAccount())
                    .amount(fullPaymentValue.setScale(2, RoundingMode.HALF_UP))
                    .reference(collectionOrder.getInternalId())
                    .channel("CM COBROS BANQUITO")
                    .serviceLevel("SEPA")
                    .externalOperation(this.sequenceService.getNextEO())
                    .documentNumber(this.sequenceService.getNextDN())
                    .transactionNumber(this.sequenceService.getNextTN())
                    .build();
            this.kafkaTemplate.send("collections_recurrement", transactionDTO);
        }
        log.info("Batch Finalizado");
        return RepeatStatus.FINISHED;
    }
    
    @Override
    public ExitStatus afterStep(StepExecution arg0) {
        return ExitStatus.COMPLETED;
    }
}
