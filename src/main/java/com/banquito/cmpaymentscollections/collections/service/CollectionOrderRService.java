package com.banquito.cmpaymentscollections.collections.service;

import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.banquito.cmpaymentscollections.collections.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class CollectionOrderRService {

    private final CollectionOrderService collectionOrderService;

    @KafkaListener(
            topics = "transaction_recurrement",
            groupId = "fooCollR",
            containerFactory = "fooCollRListener")
    public void payCollectionOrderRecurrement(TransactionDTO transactionDTO) throws NotFoundException {
        log.info("Received TransactionDTOs: {}", transactionDTO);
        collectionOrderService.payCollectionOrderResponseRecurrement(transactionDTO);
    }
}
