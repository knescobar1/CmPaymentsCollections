package com.banquito.cmpaymentscollections.collections.service;

import com.banquito.cmpaymentscollections.collections.dao.CollectionRepository;
import com.banquito.cmpaymentscollections.collections.dto.AccountDTO;
import com.banquito.cmpaymentscollections.collections.enums.CollectionEnum;
import com.banquito.cmpaymentscollections.collections.exceptions.NotFoundException;
import com.banquito.cmpaymentscollections.collections.model.Collection;
import com.banquito.cmpaymentscollections.config.BaseURLValues;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Slf4j
public class CollectionService {
  private final CollectionRepository collectionRepository;

  private final BaseURLValues baseURLValues;

  private final RestTemplate restTemplate;

  public Collection findByInternalId(String collectionId) {
    Optional<Collection> collectionOpt = this.collectionRepository.findByInternalId(collectionId);
    return collectionOpt.orElse(null);
  }

  public List<Collection> findAll() {
    return this.collectionRepository.findAll();
  }

  public List<Collection> findByGroupInternalId(String groupInternalId) {
    return this.collectionRepository.findByGroupInternalId(groupInternalId);
  }

  public List<Collection> findByPeriodicityAndGroupInternalId(
      String periodicity, String internalId) {
    return this.collectionRepository.findByPeriodicityAndGroupInternalId(periodicity, internalId);
  }

  public List<Collection> findByStateAndGroupInternalId(String state, String groupInternalId) {
    return this.collectionRepository.findByStateAndGroupInternalId(state, groupInternalId);
  }

  public List<Collection> findByServiceOfferedNameAndGroupInternalId(
      String name, String internalId) {
    return this.collectionRepository.findByServiceOfferedNameAndGroupInternalId(name, internalId);
  }

  public List<Collection> findActiveAndInactive(String groupInternalId) {
      List<String> states = new ArrayList<>();
      states.add(CollectionEnum.ACTIVE.getValue());
      states.add(CollectionEnum.INACTIVE.getValue());
      return this.collectionRepository.findByStateInAndGroupInternalId(states,groupInternalId);
  }

  public Collection create(Collection collection) throws NotFoundException {
    log.info("A Collection will be created with the following information: {}", collection);
    List<AccountDTO> listAccountDTO =
        this.obtainAccountByGroupInternalId(collection.getGroupInternalId());
    for (AccountDTO accountDTO : listAccountDTO) {
      if (accountDTO.getServiceType().equals("COM") || accountDTO.getServiceType().equals("PNC")) {
        collection.setCreditorAccount(accountDTO.getAccountNumber());
        break;
      }
    }
    collection.setInternalId(UUID.randomUUID().toString());
    collection.setCreationDate(new Date());
    collection.setCollectionId(UUID.randomUUID().toString());
    collection.setLastModifiedDate(new Date());
    collection.setState(CollectionEnum.INACTIVE.getValue());
    collection.setRecords(0);
    collection.setPaidRecords(0);
    collection.setFailedRecords(0);
    collection.setTotalCollectionValue(new BigDecimal(0));
    collection.setFailedValue(new BigDecimal(0));
    log.info("Collection {} has been created correctly", collection);
    return this.collectionRepository.save(collection);
  }

  public Collection update(Date startCollectionDate, Date endCollectionDate, String internalId)
      throws NotFoundException {
    Collection collection = this.findByInternalId(internalId);
    if (collection == null) {
      log.warn("Collection with internalId = {}  is not registed to update", internalId);
      throw new NotFoundException("Cobro no encontrado");
    }
    collection.setState(CollectionEnum.COLLECTED.getValue());
    collection.setLastModifiedDate(new Date());
    this.collectionRepository.save(collection);
    collection.setStartCollectionDate(startCollectionDate);
    collection.setEndCollectionDate(endCollectionDate);
    collection = this.createCollectionOnUpdate(collection);
    log.info("Collection {} has been updated", collection);
    return collection;
  }

  public Collection setState(String internalId) throws NotFoundException {
    Collection collection = this.findByInternalId(internalId);
    if (collection == null) {
      log.warn("Collection with internalId = {}  is not registed to update state", internalId);
      throw new NotFoundException("Cobro no encontrado");
    }
    if (collection.getState().equals(CollectionEnum.INACTIVE.getValue())) {
      collection.setState(CollectionEnum.ACTIVE.getValue());
      log.info(
          "Collection with internalId = {} changed to state {}",
          internalId,
          CollectionEnum.ACTIVE.getText());
    } else {
      collection.setState(CollectionEnum.INACTIVE.getValue());
      log.info(
          "Collection with internalId = {} changed to state {}",
          internalId,
          CollectionEnum.ACTIVE.getText());
    }
    collection.setLastModifiedDate(new Date());
    log.info("Collection {} updated state", collection);
    return this.collectionRepository.save(collection);
  }

  public List<AccountDTO> obtainAccountByGroupInternalId(String groupInternalId)
      throws NotFoundException {
    try {
      String url =
          baseURLValues.getCmClientsAccountsURL()
              + "/listAccountsByGroupInternalId/"
              + groupInternalId;
      AccountDTO[] accDTO = this.restTemplate.getForObject(url, AccountDTO[].class);
      if (accDTO == null) {
        log.error("No account found for group with id: {}", groupInternalId);
        throw new NotFoundException(
            "No se encuentra ninguna cuenta para el grupo con id: " + groupInternalId);
      } else {
        log.info("Accounts found for group id {}: {}", groupInternalId, accDTO.length);
        return List.of(accDTO);
      }
    } catch (Exception e) {
      log.error("Error getting group accounts with id: " + groupInternalId);
      throw new NotFoundException(
          "No se encuentra ninguna cuenta para el grupo con id: " + groupInternalId);
    }
  }

  public List<Collection> findAllService() {
    List<Collection> collections = this.collectionRepository.findAll();
    long index = collections.stream()
            .filter(collection -> collection.getState().equals(CollectionEnum.ACTIVE.getValue()))
            .count();
    log.info("There are {} collections with state {}", index, CollectionEnum.ACTIVE.getText());
    return collections.stream()
        .filter(collection -> collection.getState().equals(CollectionEnum.ACTIVE.getValue()))
        .collect(Collectors.toList());
  }

  private Collection createCollectionOnUpdate(Collection collection) {
    log.info("Collection {} will be update", collection);
    collection.setCreationDate(new Date());
    collection.setLastModifiedDate(new Date());
    collection.setState(CollectionEnum.ACTIVE.getValue());
    collection.setInternalId(UUID.randomUUID().toString());
    collection.setRecords(0);
    collection.setPaidRecords(0);
    collection.setFailedRecords(0);
    collection.setTotalCollectionValue(new BigDecimal(0));
    collection.setFailedValue(new BigDecimal(0));
    log.info("Collection has been updated: {}", collection);
    return this.collectionRepository.save(collection);
  }
}
