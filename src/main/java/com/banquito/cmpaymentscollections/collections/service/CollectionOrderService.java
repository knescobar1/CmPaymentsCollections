package com.banquito.cmpaymentscollections.collections.service;

import com.banquito.cmpaymentscollections.collections.dao.CollectionOrderRepository;
import com.banquito.cmpaymentscollections.collections.dao.CollectionRepository;
import com.banquito.cmpaymentscollections.collections.dto.JournalDTO;
import com.banquito.cmpaymentscollections.collections.dto.TransactionDTO;
import com.banquito.cmpaymentscollections.collections.enums.CollectionEnum;
import com.banquito.cmpaymentscollections.collections.enums.CollectionOrderEnum;
import com.banquito.cmpaymentscollections.collections.enums.PaymentWayEnum;
import com.banquito.cmpaymentscollections.collections.enums.ProcessingStateEnum;
import com.banquito.cmpaymentscollections.collections.exceptions.*;
import com.banquito.cmpaymentscollections.collections.model.Collection;
import com.banquito.cmpaymentscollections.collections.model.CollectionCustomer;
import com.banquito.cmpaymentscollections.collections.model.CollectionOrder;
import com.banquito.cmpaymentscollections.config.ApplicationValues;
import com.banquito.cmpaymentscollections.config.BaseURLValues;
// import com.google.auth.oauth2.ServiceAccountCredentials;
// import com.google.cloud.storage.BlobId;
// import com.google.cloud.storage.BlobInfo;
// import com.google.cloud.storage.Storage;
// import com.google.cloud.storage.StorageOptions;
import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
@Slf4j
@EnableAsync
public class CollectionOrderService {
  private final KafkaTemplate<String, Object> kafkaTemplate;
  private final CollectionOrderRepository collectionOrderRepository;
  private final CollectionRepository collectionRepository;
  private final RestTemplate restTemplate;
  private final BaseURLValues baseURLs;
  private final SequenceCollService sequenceService;

  @Autowired private ApplicationValues appValues;

  private static final String SERVICE_ACCOUNT_JSON_PATH = "src/main/resources/storage_key.json";

  public Collection findCollectionById(String collectionId) {
    Optional<Collection> collectionOpt = this.collectionRepository.findByInternalId(collectionId);
    return collectionOpt.orElse(null);
  }

  public CollectionOrder findByInternalId(String internalId) {
    Optional<CollectionOrder> collectionOrderOpt =
        this.collectionOrderRepository.findByInternalId(internalId);
    return collectionOrderOpt.orElse(null);
  }

  public List<CollectionOrder> findByCollectionId(String collectionId) {
    return this.collectionOrderRepository.findByCollectionIdOrderByCustomerFullName(collectionId);
  }

  public List<CollectionOrder> findByReferenceIdAndCollectionId(
      String referenceId, String collectionId) {
    return this.collectionOrderRepository.findByReferenceIdAndCollectionId(
        referenceId, collectionId);
  }

  public List<CollectionOrder> findByStateAndCollectionIdOrderByCustomerName(
      String state, String collectionId) {
    return this.collectionOrderRepository.findByStateAndCollectionIdOrderByCustomerFullName(
        state, collectionId);
  }

  public List<CollectionOrder> findBetweenCollectedDateAndCollectionId(
      Date startCollectedDate, Date endCollectedDate, String companyId) {
    return this.collectionOrderRepository
        .findByCollectedDateBetweenAndCollectionIdOrderByCustomerFullName(
            startCollectedDate, endCollectedDate, companyId);
  }

  public List<CollectionOrder> findByDebtorAccount(BigInteger debtorAccount) {
    return this.collectionOrderRepository.findByDebtorAccountAndStateAndPaymentWay(
        debtorAccount, CollectionOrderEnum.TOPAY.getValue(), PaymentWayEnum.MANUAL.getValue());
  }

  public List<CollectionOrder> getCollectionOrderRecurrementToPay() {
    return this.collectionOrderRepository.findByStateAndPaymentWay(
        CollectionOrderEnum.TOPAY.getValue(), PaymentWayEnum.RECURRENT.getValue());
  }

  public List<CollectionOrder> getCollectionOrderPaymentWay(
      BigInteger debtorAccount, String paymentWay) {
    return this.collectionOrderRepository.findByDebtorAccountAndPaymentWay(
        debtorAccount, paymentWay);
  }

  @Async
  public void createOne(CollectionOrder collectionOrder) throws NotFoundException {
    boolean exists = false;
    BigDecimal lastOrderAmount = new BigDecimal(0);
    Collection collectionDB = this.findCollectionById(collectionOrder.getCollectionId());
    if (collectionDB == null) {
      log.warn("Cobro con id = {} no encontrado", collectionOrder.getCollectionId());
      throw new NotFoundException("Cobro no encontrado");
    }
    List<CollectionOrder> listCollectionOrder =
        this.findByReferenceIdAndCollectionId(
            collectionOrder.getReferenceId(), collectionOrder.getCollectionId());
    for (CollectionOrder collectionOrderItemDB : listCollectionOrder) {
      if (collectionOrderItemDB
              .getStartCollectionDate()
              .equals(collectionDB.getStartCollectionDate())
          && collectionOrderItemDB
              .getEndCollectionDate()
              .equals(collectionDB.getEndCollectionDate())) {
        exists = true;
      }
    }
    if (!exists) {
      List<CollectionOrder> collectionOrdersDB =
          this.findByReferenceIdAndCollectionId(
              collectionOrder.getReferenceId(), collectionOrder.getCollectionId());
      if (!collectionOrdersDB.isEmpty()) {
        boolean firstIterationFlag = true;
        Date lastOrderDate = null;
        for (CollectionOrder order : collectionOrdersDB) {
          if (firstIterationFlag) {
            lastOrderDate = order.getEndCollectionDate();
            if (order.getState().equals(CollectionOrderEnum.TOPAY.getValue())) {
              lastOrderAmount = order.getAmount();
            }
            firstIterationFlag = false;
          } else {
            if (order.getEndCollectionDate().after(lastOrderDate)) {
              lastOrderDate = order.getEndCollectionDate();
              if (order.getState().equals(CollectionOrderEnum.TOPAY.getValue())) {
                lastOrderAmount = order.getAmount();
              } else {
                lastOrderAmount = new BigDecimal(0);
              }
            }
          }
        }
      }
      collectionOrder.setAmount(
          collectionOrder.getAmount().add(lastOrderAmount).setScale(2, RoundingMode.HALF_UP));
      collectionOrder.setCollectionId(collectionDB.getInternalId());
      collectionOrder.setInternalId(UUID.randomUUID().toString());
      collectionOrder.setStartCollectionDate(collectionDB.getStartCollectionDate());
      collectionOrder.setEndCollectionDate(collectionDB.getEndCollectionDate());
      collectionOrder.setState(CollectionOrderEnum.TOPAY.getValue());
      collectionOrder.setProcessingState(ProcessingStateEnum.CREATED.getValue());
      log.info("Creando Cobro");
      this.collectionOrderRepository.save(collectionOrder);
      log.info("Cobro creado");
    } else {
      log.warn("Ya existe una orden con ese numero de referencia");
    }
  }

  @Async
  public void validateCSVFile(MultipartFile file) throws CSVFileException {
    List<String> headers = new ArrayList<>();
    headers.add("CONTRAPARTIDA CODIGO DEL CLIENTE");
    headers.add("NUMERO DE DOCUMENTO DEL CLIENTE");
    headers.add("TIPO DE DOCUMENTO DEL CLIENTE");
    headers.add("NOMBRES DEL CLIENTE");
    headers.add("NUMERO DE CUENTA");
    headers.add("TIPO DE PAGO");
    headers.add("VALOR A PAGAR");
    headers.add("REFERENCIA");
    try {
      CsvParserSettings settings = new CsvParserSettings();
      InputStream inputStream = file.getInputStream();
      settings.setHeaderExtractionEnabled(true);
      CsvParser parser = new CsvParser(settings);
      List<Record> parseAllRecords = parser.parseAllRecords(inputStream);
      for (String header : headers) {
        try {
          parseAllRecords.get(0).getString(header);
        } catch (Exception e) {
          log.error("No existe el encabezado " + header + " en el archivo.");
          throw new CSVFileException("No existe el encabezado " + header + " en el archivo.");
        }
      }
      for (int i = 0; i < parseAllRecords.size(); i++) {
        for (int j = 0; j < headers.size(); j++) {
          if (validateColumn(parseAllRecords.get(i), headers.get(j))) {
            log.error(
                "El dato " + headers.get(j) + " en la columna " + (i + 1) + " es obligatorio");
            throw new CSVFileException(
                "El dato " + headers.get(j) + " en la columna " + (i + 1) + " es obligatorio");
          }
        }
      }
    } catch (IOException e) {
      log.error(
          "El archivo no se encuentra en el formato correcto. La separación debe estar dada por"
              + " comas (,)");
      throw new CSVFileException(
          "El archivo no se encuentra en el formato correcto. La separación debe estar dada por"
              + " comas (,)");
    }
  }

  @Async
  public void createWithList(List<CollectionOrder> collectionOrder) throws NotFoundException {
    for (CollectionOrder collectionOrderItem : collectionOrder) {
      boolean exists = false;
      BigDecimal lastOrderAmount = new BigDecimal(0);
      Collection collectionDB = this.findCollectionById(collectionOrderItem.getCollectionId());
      if (collectionDB == null) {
        log.warn("Cobro con internalId = {} no encontrado", collectionOrderItem.getCollectionId());
        throw new NotFoundException("Cobro no encontrado");
      }
      List<CollectionOrder> listCollectionOrder =
          this.findByReferenceIdAndCollectionId(
              collectionOrderItem.getReferenceId(), collectionOrderItem.getCollectionId());
      for (CollectionOrder collectionOrderItemDB : listCollectionOrder) {
        if (collectionOrderItemDB
                .getStartCollectionDate()
                .equals(collectionDB.getStartCollectionDate())
            && collectionOrderItemDB
                .getEndCollectionDate()
                .equals(collectionDB.getEndCollectionDate())) {
          exists = true;
        }
      }
      if (!exists) {
        List<CollectionOrder> collectionOrdersDB =
            this.findByReferenceIdAndCollectionId(
                collectionOrderItem.getReferenceId(), collectionOrderItem.getCollectionId());
        if (!collectionOrdersDB.isEmpty()) {
          boolean firstIterationFlag = true;
          Date lastOrderDate = null;
          for (CollectionOrder order : collectionOrdersDB) {
            if (firstIterationFlag) {
              lastOrderDate = order.getEndCollectionDate();
              if (order.getState().equals(CollectionOrderEnum.TOPAY.getValue())) {
                lastOrderAmount = order.getAmount();
              }
              firstIterationFlag = false;
            } else {
              if (order.getEndCollectionDate().after(lastOrderDate)) {
                lastOrderDate = order.getEndCollectionDate();
                if (order.getState().equals(CollectionOrderEnum.TOPAY.getValue())) {
                  lastOrderAmount = order.getAmount();
                } else {
                  lastOrderAmount = new BigDecimal(0);
                }
              }
            }
          }
        }
        collectionOrderItem.setAmount(
            collectionOrderItem.getAmount().add(lastOrderAmount).setScale(2, RoundingMode.HALF_UP));
        collectionOrderItem.setCollectionId(collectionDB.getInternalId());
        collectionOrderItem.setInternalId(UUID.randomUUID().toString());
        collectionOrderItem.setStartCollectionDate(collectionDB.getStartCollectionDate());
        collectionOrderItem.setEndCollectionDate(collectionDB.getEndCollectionDate());
        collectionOrderItem.setProcessingState(ProcessingStateEnum.CREATED.getValue());
        collectionOrderItem.setState(CollectionOrderEnum.TOPAY.getValue());
        collectionDB.setRecords(collectionDB.getRecords() + 1);
        collectionDB.setTotalCollectionValue(
            collectionDB.getTotalCollectionValue().add(collectionOrderItem.getAmount()));
        this.collectionOrderRepository.save(collectionOrderItem);
        log.info("Cobro creado");
      } else {
        log.warn("Ya existe una orden con ese numero de referencia");
      }
    }
  }

  public CollectionOrder collectedAmount(String internalId) throws NotFoundException {
    CollectionOrder collectionOrder = this.findByInternalId(internalId);
    if (collectionOrder == null) {
      log.warn("Orden de cobro con Id: {} no encontrado ", internalId);
      throw new NotFoundException("Orden de cobro con Id: " + internalId + "no encontrado");
    }
    Date nowDate = new Date();
    if (nowDate.before(collectionOrder.getEndCollectionDate())
        || nowDate.equals(collectionOrder.getEndCollectionDate())) {
      collectionOrder.setState(CollectionOrderEnum.PAID.getValue());
      log.info(
          "La Orden de cobro con Id: {}  se cambio a estado: {}",
          internalId,
          CollectionOrderEnum.PAID.getText());
    } else {
      collectionOrder.setState(CollectionOrderEnum.READY.getValue());
      log.info(
          "La Orden de cobro con Id: {}  se cambio a estado: {}",
          internalId,
          CollectionOrderEnum.READY.getText());
    }
    collectionOrder.setCollectedDate(nowDate);
    collectionOrder.setPaid(collectionOrder.getAmount());
    log.info("Orden de cobro con Id: " + internalId + "actualizada");
    return this.collectionOrderRepository.save(collectionOrder);

  }

  public void payCollectionOrder(String internalId)
      throws NotFoundException, NotEnoughtFundsException {
    CollectionOrder collectionOrderDb = this.findByInternalId(internalId);
    BigDecimal fullPaymentValue = new BigDecimal(0);
    if (collectionOrderDb == null) {
      log.warn("Orden de cobro con Id: {} no encontrado ", internalId);
      throw new NotFoundException("Orden de Cobro no encontrada");
    }

    if (collectionOrderDb.getState().equals(CollectionOrderEnum.PAID.getValue())) {
      throw new CollectionServiceException("La orden ya se encuentra pagada");
    }
    if (collectionOrderDb.getPending().compareTo(new BigDecimal(0)) == 0) {
      fullPaymentValue = collectionOrderDb.getAmount();
    } else {
      fullPaymentValue = collectionOrderDb.getPending();
    }
    Collection collectionDb = this.findCollectionById(collectionOrderDb.getCollectionId());
    TransactionDTO transactionDTO =
        TransactionDTO.builder()
            .debtorAccountNumber(collectionOrderDb.getDebtorAccount())
            .creditorAccountNumber(collectionDb.getCreditorAccount())
            .amount(fullPaymentValue.setScale(2, RoundingMode.HALF_UP))
            .reference(collectionOrderDb.getInternalId())
            .channel("CM COBROS BANQUITO")
            .serviceLevel("SEPA")
            .externalOperation(this.sequenceService.getNextEO())
            .documentNumber(this.sequenceService.getNextDN())
            .transactionNumber(this.sequenceService.getNextTN())
            .build();
    this.kafkaTemplate.send("collections_wesos", transactionDTO);
  }

  //  @KafkaListener(
  //      topics = "transaction_wesos",
  //      groupId = "fooColl2",
  //      containerFactory = "fooCollListener")
  public void payCollectionOrderResponse(TransactionDTO transactionDTO) throws NotFoundException {
    log.info("Transaction recibed: {}", transactionDTO);
    String internalId = transactionDTO.getReference();
    BigDecimal fullPaymentValue = transactionDTO.getAmount();
    CollectionOrder collectionOrderDb = this.findByInternalId(internalId);
    if (collectionOrderDb == null) {
      log.warn("Orden de cobro con Id: {} no encontrado ", internalId);
      throw new NotFoundException("Orden de Cobro no encontrada");
    }
    Collection collectionDb = this.findCollectionById(collectionOrderDb.getCollectionId());
    if (collectionDb == null) {
      log.warn("La coleccion con Id: {} no encontrada ", collectionOrderDb.getCollectionId());
      throw new NotFoundException("Cobro no encontrado");
    }
    try {
      log.info("Entró Journal");
      JournalDTO journalDTO =
          JournalDTO.builder()
              .journalId("")
              .amount(collectionOrderDb.getAmount())
              .transactionReference(transactionDTO.getTransactionNumber())
              .description(collectionOrderDb.getReference())
              .transactionDate(new Date())
              .type("COLLECTION")
              .build();
      HttpEntity<JournalDTO> journalTransaction = new HttpEntity<>(journalDTO);
      JournalDTO journalCreated =
          this.restTemplate.postForObject(
              baseURLs.getCmAccountingJournalEntryURL(), journalTransaction, JournalDTO.class);
      log.info("pasó journal");
      CollectionOrder collectionOrder = this.collectedAmount(internalId);
      collectionDb.setPaidRecords(collectionDb.getPaidRecords() + 1);
      collectionOrder.setJournalId(journalCreated.getJournalId());
      this.collectionOrderRepository.save(collectionOrder);
      this.collectionRepository.save(collectionDb);
    } catch (Exception e) {
      log.error("Error al crear el journal", e);
      System.out.println(e.getMessage());
      collectionOrderDb.setState(CollectionOrderEnum.FAILED.getValue());
      collectionDb.setFailedRecords(collectionDb.getFailedRecords() + 1);
      collectionDb.setFailedValue(collectionDb.getFailedValue().add(fullPaymentValue));
      log.error("Error al realizar el pago");
      this.collectionOrderRepository.save(collectionOrderDb);
      this.collectionRepository.save(collectionDb);
    }
  }

  public void payCollectionOrderResponseRecurrement(TransactionDTO transactionDTO)
      throws NotFoundException {
    String internalId = transactionDTO.getReference();
    BigDecimal fullPaymentValue = transactionDTO.getAmount();
    CollectionOrder collectionOrderDb = this.findByInternalId(internalId);
    if (collectionOrderDb == null) {
      log.warn("Orden de cobro con Id: {} no encontrado ", internalId);
      throw new NotFoundException("Orden de Cobro no encontrada");
    }
    Collection collectionDb = this.findCollectionById(collectionOrderDb.getCollectionId());
    if (collectionDb == null) {
      log.warn("La coleccion con Id: {} no encontrada ", collectionOrderDb.getCollectionId());
      throw new NotFoundException("Cobro no encontrado");
    }
    try {
      JournalDTO journalDTO =
          JournalDTO.builder()
              .journalId("")
              .amount(collectionOrderDb.getAmount())
              .transactionReference(transactionDTO.getTransactionNumber())
              .description(transactionDTO.getReference())
              .transactionDate(new Date())
              .type("COLLECTION")
              .build();
      HttpEntity<JournalDTO> journalTransaction = new HttpEntity<>(journalDTO);
      JournalDTO journalCreated =
          this.restTemplate.postForObject(
              baseURLs.getCmAccountingJournalEntryURL(), journalTransaction, JournalDTO.class);
      collectionOrderDb.setJournalId(journalCreated.getJournalId());
      collectionDb.setPaidRecords(collectionDb.getPaidRecords() + 1);

      if (fullPaymentValue.compareTo(collectionOrderDb.getAmount()) == 0) {
        collectionOrderDb.setState(CollectionOrderEnum.PAID.getValue());
        collectionOrderDb.setPaid(collectionOrderDb.getAmount());
      } else {
        collectionOrderDb.setPaid(collectionOrderDb.getPaid().add(fullPaymentValue));
        if ((collectionOrderDb.getPaid().add(fullPaymentValue))
                .compareTo(collectionOrderDb.getAmount())
            == 0) {
          collectionOrderDb.setState(CollectionOrderEnum.PAID.getValue());
          collectionOrderDb.setPending(new BigDecimal(0));
        } else {
          collectionOrderDb.setPending(
              collectionOrderDb.getAmount().subtract(collectionOrderDb.getPaid()));
        }
      }

      this.collectionOrderRepository.save(collectionOrderDb);
      this.collectionRepository.save(collectionDb);
    } catch (Exception e) {
      collectionOrderDb.setState(CollectionOrderEnum.FAILED.getValue());
      collectionDb.setFailedRecords(collectionDb.getFailedRecords() + 1);
      collectionDb.setFailedValue(collectionDb.getFailedValue().add(fullPaymentValue));
      log.error("Error al realizar el pago");
      this.collectionOrderRepository.save(collectionOrderDb);
      this.collectionRepository.save(collectionDb);
    }
  }

  public void readyCollection(List<String> internalIds) throws NotFoundException {
    for (String collectionOrderId : internalIds) {
      CollectionOrder collectionOrder = this.findByInternalId(collectionOrderId);
      if (collectionOrder == null) {
        log.warn("Cobro con internalId = {} no encontrado", collectionOrderId);
        throw new NotFoundException("Orden de Cobro no encontrada");
      }
      Date nowDate = new Date();
      if (nowDate.after(collectionOrder.getEndCollectionDate())
          && collectionOrder.getState().equals(CollectionOrderEnum.PAID.getValue())) {
        collectionOrder.setState(CollectionOrderEnum.READY.getValue());
      }
      this.collectionOrderRepository.save(collectionOrder);
      log.info("Orden de cobro con Id: " + collectionOrderId + " actualizada");
    }
  }

  public String uploadToPayCsv(MultipartFile file, String collectionId)
      throws NotFoundException, IOException {
    Optional<Collection> collectionOpt = this.collectionRepository.findByInternalId(collectionId);
    if (collectionOpt.isEmpty()) {
      throw new NotFoundException("No existe el cobro con el id:" + collectionId);
    }

    //    uploadFileToBucket(file);

    Collection collectionDB = collectionOpt.get();
    List<CollectionOrder> orders = new ArrayList<>();
    CsvParserSettings settings = new CsvParserSettings();
    InputStream inputStream;
    try {
      inputStream = file.getInputStream();
    } catch (IOException e) {
      log.error("Error al leer el archivo");
      throw new CSVFileException("Error al leer el archivo");
    }
    settings.setHeaderExtractionEnabled(true);
    CsvParser parser = new CsvParser(settings);
    List<Record> parseAllRecords = parser.parseAllRecords(inputStream);
    parseAllRecords.forEach(
        recordC -> {
          CollectionOrder collectionsOrder =
              CollectionOrder.builder()
                  .referenceId(recordC.getString("CONTRAPARTIDA CODIGO DEL CLIENTE"))
                  .collectionId(collectionDB.getInternalId())
                  .customer(
                      CollectionCustomer.builder()
                          .customerId(recordC.getString("NUMERO DE DOCUMENTO DEL CLIENTE"))
                          .typeCustomerId(recordC.getString("TIPO DE DOCUMENTO DEL CLIENTE"))
                          .fullName(recordC.getString("NOMBRES DEL CLIENTE"))
                          .build())
                  .debtorAccount(recordC.getBigInteger("NUMERO DE CUENTA"))
                  .paymentWay(recordC.getString("TIPO DE PAGO"))
                  .amount(recordC.getBigDecimal("VALOR A PAGAR"))
                  .reference(recordC.getString("REFERENCIA"))
                  .build();
          orders.add(collectionsOrder);
        });
    try {
      this.createWithList(orders);
    } catch (NotFoundException e) {
      log.error("Error al cargar el archivo");
      return e.getMessage();
    }
    return "Archivo cargado exitosamente";
  }

  //  private void uploadFileToBucket(MultipartFile file) throws IOException {
  //    Storage storage =
  //        StorageOptions.newBuilder()
  //            .setProjectId(appValues.getProjectId())
  //            .setCredentials(
  //                ServiceAccountCredentials.fromStream(
  //                    new FileInputStream(SERVICE_ACCOUNT_JSON_PATH)))
  //            .build()
  //            .getService();
  //    if (file.getOriginalFilename() != null) {
  //      BlobId blobId = BlobId.of(appValues.getBucketName(), file.getOriginalFilename());
  //      BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
  //      byte[] content = file.getBytes();
  //      storage.createFrom(blobInfo, new ByteArrayInputStream(content));
  //      log.info("El archivo {} se almacenó correctamente", file.getName());
  //    }
  //    log.error("El archivo {} no se pudo almacenar", file.getName());
  //  }

  public CollectionOrder updateCollectionOrder(CollectionOrder collectionOrder)
      throws BadRequestException {
    CollectionOrder collectionOrderDb = this.findByInternalId(collectionOrder.getInternalId());
    try {
      if (collectionOrderDb.getState().equals(CollectionOrderEnum.PAID.getValue())) {
        log.warn("No se puede actualizar un cobro pagado");
        throw new BadRequestException("No se puede actualizar un cobro pagado");
      }
      collectionOrderDb.setDebtorAccount(collectionOrder.getDebtorAccount());
      collectionOrderDb.setPaymentWay(collectionOrder.getPaymentWay());
      collectionOrderDb.setAmount(collectionOrder.getAmount());
      collectionOrderDb.setReference(collectionOrder.getReference());
      this.collectionOrderRepository.save(collectionOrderDb);
    } catch (Exception e) {
      log.warn("Cobro con internalId = {} no encontrado", collectionOrder.getInternalId());
      throw new CSVFileException("Orden de Cobro no encontrada para actualizar");
    } catch (BadRequestException e) {
      log.warn("No se puede actualizar un cobro pagado");
      throw new BadRequestException(e.getMessage());
    }
    return collectionOrderDb;
  }

  public void deleteCollectionOrder(String internalId)
      throws NotFoundException, BadRequestException {
    CollectionOrder collectionOrderDb = this.findByInternalId(internalId);
    if (collectionOrderDb == null) {
      log.warn("Cobro con internalId = {} no encontrado", internalId);
      throw new NotFoundException("Orden de Cobro no encontrada");
    } else if (collectionOrderDb.getState().equals(CollectionOrderEnum.PAID.getValue())) {
      log.warn("No se puede eliminar un cobro pagado");
      throw new BadRequestException("No se puede eliminar un cobro pagado");
    }
    this.collectionOrderRepository.delete(collectionOrderDb);
  }

  public List<CollectionOrder> reportCollection(
      String serviceName, List<String> state, Date dateFrom, Date dateTo) {
    List<Collection> collections = this.collectionRepository.findByServiceOfferedName(serviceName);
    List<String> collectionIds =
        collections.stream().map(Collection::getInternalId).collect(Collectors.toList());
    return this.collectionOrderRepository
        .findByCollectionIdInAndStateInAndStartCollectionDateBetweenAndProcessingState(
            collectionIds, state, dateFrom, dateTo, ProcessingStateEnum.PROCESSED.getValue());
  }

  public String pay(String referenceId, String collectionId) throws NotFoundException {
    Date today = new Date();
    List<CollectionOrder> orders =
        this.collectionOrderRepository.findByReferenceIdAndCollectionId(referenceId, collectionId);
    if (orders.isEmpty()) {
      log.warn("No se ha encontrado ninguna orden de cobro con la referencia {}", referenceId);
      return "No se ha encontrado ninguna orden de cobro con la referencia " + referenceId;
    }
    for (CollectionOrder order : orders) {
      if (today.after(order.getStartCollectionDate())
          && today.before(order.getEndCollectionDate())
          && order.getState().equals(CollectionOrderEnum.TOPAY.getValue())) {
        this.payCollectionOrder(order.getInternalId());
        log.info("Se ha pagado con exito la orden de pago con referencia : {}", referenceId);
        return "pagado";
      }
    }
    log.warn("No se ha encontrado ninguna orden de cobro con la referencia {}", referenceId);
    return "No se ha encontrado ninguna orden de cobro con la referencia " + referenceId;
  }

  public String processCollectionOrder(String collectionId) {
    try {
      Optional<Collection> collectionOpt = this.collectionRepository.findByInternalId(collectionId);
      if (collectionOpt.isEmpty()) {
        throw new NotFoundException("No existe el Cobro con el id:" + collectionId);
      }
      Collection collectionDB = collectionOpt.get();
      collectionDB.setState(CollectionEnum.PROCESS.getValue());
      this.collectionRepository.save(collectionDB);
      List<CollectionOrder> collectionOrders =
          this.findByCollectionId(collectionDB.getInternalId());
      for (CollectionOrder collectionOrder : collectionOrders) {
        collectionOrder.setProcessingState(ProcessingStateEnum.PROCESSED.getValue());
        this.collectionOrderRepository.save(collectionOrder);
      }
      return "Se procesaron con éxito las ordenes de Cobros";
    } catch (Exception e) {
      throw new CSVFileException(
          "No se encontró ninguna Orden de cobro con el Cobro: " + collectionId);
    }
  }

  public void changePaymentWay(String internalId) {
    CollectionOrder collectionOrderDb = this.findByInternalId(internalId);
    if (collectionOrderDb.getPaymentWay().equals(PaymentWayEnum.MANUAL.getValue()))
      collectionOrderDb.setPaymentWay(PaymentWayEnum.RECURRENT.getValue());
    else collectionOrderDb.setPaymentWay(PaymentWayEnum.MANUAL.getValue());
    this.collectionOrderRepository.save(collectionOrderDb);
  }

  public List<CollectionOrder> obtainOrderbyCollectionServiceName(
      String serviceName, String referenceId) {
    List<Collection> collections = this.collectionRepository.findByServiceOfferedName(serviceName);
    List<CollectionOrder> collectionOrders = new ArrayList<>();
    Date today = new Date();
    for (Collection collection : collections) {
      List<CollectionOrder> orders =
          this.collectionOrderRepository.findByReferenceIdAndCollectionId(
              referenceId, collection.getInternalId());
      List<CollectionOrder> ordersToAdd = new ArrayList<>();
      for (CollectionOrder order : orders) {
        if (order.getState().equals(CollectionOrderEnum.TOPAY.getValue())
            && today.after(order.getStartCollectionDate())
            && today.before(order.getEndCollectionDate())) {
          ordersToAdd.add(order);
        }
      }
      collectionOrders.addAll(ordersToAdd);
      if (!orders.isEmpty()) {
        return collectionOrders;
      }
    }
    return collectionOrders;
  }

  public List<CollectionOrder> obtainByCustomerAndState(String state, String customerId) {
    return this.collectionOrderRepository.findByProcessingStateAndCustomerCustomerId(
        state, customerId);
  }

  private Boolean validateColumn(Record column, String header) {
    Boolean empty = false;
    try {
      if (column.getString(header).isEmpty()) {
        empty = true;
      }
      return empty;
    } catch (Exception e) {
      return true;
    }
  }
}
