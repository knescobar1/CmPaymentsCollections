package com.banquito.cmpaymentscollections.collections.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepositDTO {
  private String referenceId;

  private BigInteger accountNumber;

  private BigDecimal amount;

  private String notes;

  private String transactionChannel;

  private String documentNumber;

  private String transactionNumber;
}
