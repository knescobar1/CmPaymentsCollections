package com.banquito.cmpaymentscollections;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
//@EnableScheduling
@EnableAsync
public class CmPaymentsCollectionsApplication {
/*
  @Autowired
	JobLauncher jobLauncher;

	@Autowired
    @Qualifier("processTransactionJob")
    Job job1; */
  public static void main(String[] args) {
    SpringApplication.run(CmPaymentsCollectionsApplication.class, args);
  }
  /*

  @Scheduled(cron = "0 * 0/12 1/1 * ? *")
    public void perform() throws Exception {
        JobParameters params = new JobParametersBuilder()
                .addString("processTransactionJob", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(job1, params);
    } */
}

