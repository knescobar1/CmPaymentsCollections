package com.banquito.cmpaymentscollections.collections.service;

import com.banquito.cmpaymentscollections.collections.dao.CollectionRepository;
import com.banquito.cmpaymentscollections.collections.dto.AccountDTO;
import com.banquito.cmpaymentscollections.collections.enums.CollectionEnum;
import com.banquito.cmpaymentscollections.collections.exceptions.NotFoundException;
import com.banquito.cmpaymentscollections.collections.model.Collection;
import com.banquito.cmpaymentscollections.collections.model.CollectionServiceOffered;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CollectionServiceTest {

  @Mock private CollectionRepository collectionRepository;

  @InjectMocks private CollectionService collectionService;

  private Collection collectionDB;

  private Collection collectionNew;

  private AccountDTO accountDTO;

  @BeforeEach
  void setup() {
    BigInteger creditorAccount = new BigInteger("445594876422143");
    this.collectionDB =
        Collection.builder()
            .channel("WEB")
            .serviceOffered(
                CollectionServiceOffered.builder()
                    .name("Agua Potable")
                    .description("Cobros de Agua Potable")
                    .build())
            .groupInternalId("5ba4d84b-cd03-448e-9284-ae743d5afe8a")
            .internalId("f3c83802-a16b-417f-a448-43aef9d29234")
            .creditorAccount(creditorAccount)
            .startCollectionDate(new Date())
            .endCollectionDate(new Date())
            .periodicity("Mensual")
            .reference("Cobro Mensual")
            .state("ACT")
            .build();

    this.collectionNew =
        Collection.builder()
            .channel("WEB")
            .serviceOffered(
                CollectionServiceOffered.builder()
                    .name("Agua Potable")
                    .description("Cobros de Agua Potable")
                    .build())
            .groupInternalId("5ba4d84b-cd03-448e-9284-ae743d5afe8a")
            .internalId("f3c83802-a16b-417f-a448-43aef9d23334")
            .creditorAccount(creditorAccount)
            .startCollectionDate(new Date())
            .endCollectionDate(new Date())
            .periodicity("Mensual")
            .reference("Cobro Mensual")
            .state("ACT")
            .build();

    this.accountDTO =
        AccountDTO.builder()
            .accountNumber(creditorAccount)
            .accountType("AHO")
            .serviceType("COM")
            .accountingBalance(BigDecimal.valueOf(1000.00))
            .availableBalance(BigDecimal.valueOf(1000.00))
            .accountState("ACT")
            .groupInternalId("5ba4d84b-cd03-448e-9284-ae743d5afe8a")
            .build();
  }

  @DisplayName("Test for findByInternalId method")
  @Test
  void givenInternalId_whenFindByInternalId_thenCollections() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    Collection collection = collectionService.findByInternalId(collectionDB.getInternalId());
    assertEquals(collection.getInternalId(), collectionDB.getInternalId(), "Elementos Iguales");
  }

  @DisplayName("Test for findByGroupInternalId method")
  @Test
  void givenGroupInternalId_whenFindByGroupInternalId_thenCollections() {
    given(collectionRepository.findByGroupInternalId(any())).willReturn(List.of(collectionDB));
    List<Collection> collections =
        collectionService.findByGroupInternalId(collectionDB.getGroupInternalId());
    assertThat(collections).isNotNull();
    assertThat(collections.contains(collectionDB)).isNotNull();
  }

  @DisplayName("Test for findByPeriodicityAndGroupInternalId method")
  @Test
  void givenPeriodicity_whenFindByPeriodicityAndGroupInternalId_thenCollections() {
    given(collectionRepository.findByPeriodicityAndGroupInternalId(any(), any()))
        .willReturn(List.of(collectionDB));
    List<Collection> collections =
        collectionService.findByPeriodicityAndGroupInternalId(
            collectionDB.getPeriodicity(), collectionDB.getGroupInternalId());
    assertThat(collections).isNotNull();
    assertThat(collections.contains(collectionDB)).isNotNull();
  }

  @DisplayName("Test for findByStateAndGroupInternalId method")
  @Test
  void givenState_whenFindByStateAndGroupInternalId_thenCollections() {
    given(collectionRepository.findByStateAndGroupInternalId(any(), any()))
        .willReturn(List.of(collectionDB));
    List<Collection> collections =
        collectionService.findByStateAndGroupInternalId(
            collectionDB.getState(), collectionDB.getGroupInternalId());
    assertThat(collections).isNotNull();
    assertThat(collections.contains(collectionDB)).isNotNull();
  }

  @DisplayName("Test for findByServiceOfferedNameAndGroupInternalId method")
  @Test
  void givenServiceName_whenFindByServiceOfferedNameAndGroupInternalId_thenCollections() {
    given(collectionRepository.findByServiceOfferedNameAndGroupInternalId(any(), any()))
        .willReturn(List.of(collectionDB));
    List<Collection> collections =
        collectionService.findByServiceOfferedNameAndGroupInternalId(
            collectionDB.getServiceOffered().getName(), collectionDB.getGroupInternalId());
    assertThat(collections).isNotNull();
    assertThat(collections.contains(collectionDB)).isNotNull();
  }

  @DisplayName("Test for findActiveAndInactive method")
  @Test
  void givenState_whenFindActiveAndInactive_thenCollection() {
    given(collectionRepository.findByStateInAndGroupInternalId(any(), any()))
        .willReturn(List.of(collectionDB));
    List<Collection> collections =
        collectionService.findActiveAndInactive(collectionDB.getGroupInternalId());
    assertThat(collections).isNotNull();
    assertThat(collections.contains(collectionDB)).isNotNull();
  }

  @DisplayName("Test for findAllService method")
  @Test
  void givenCollectionLis_whenFindAllService_thenAllCollections() {
    List<Collection> collections = collectionService.findAllService();
    assertThat(collections).isNotNull();
    assertThat(collections.contains(collectionDB)).isNotNull();
  }

  @DisplayName("Test for findAll method")
  @Test
  void givenCollectionList_whenFindAll_thenReturnCollectionList() {
    List<Collection> collections = List.of(collectionDB, collectionNew);
    given(collectionRepository.findAll()).willReturn(collections);
    List<Collection> obtainedCollection = collectionService.findAll();
    assertThat(obtainedCollection).isNotNull();
    assertThat(obtainedCollection.size()).isEqualTo(2);
  }

  @DisplayName("Test for create method")
  @SneakyThrows
  @Test
  void givenCollection_whenCreate_thenReturnCollection() {
    //
    // when(collectionService.obtainAccountByGroupInternalId(any())).thenReturn(List.of(accountDTO));
    //    collectionDB.setState(CollectionEnum.INACTIVE.getValue());
    //    given(collectionRepository.save(any())).willReturn(collectionDB);
    //    Collection obtainedCollection = collectionService.create(collectionTets);
    //    assertThat(obtainedCollection).isNotNull();
    //    assertThat(obtainedCollection.getState()).isEqualTo(CollectionEnum.INACTIVE.getValue());
  }

  @DisplayName("Test for update method")
  @SneakyThrows
  @Test
  void givenStartDateEndDateAndInternalId_whenUpdate_thenReturnCollection() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    given(collectionRepository.save(any())).willReturn(collectionDB);
    Collection obtainedCollection =
        collectionService.update(new Date(), new Date(), collectionDB.getInternalId());
    assertThat(obtainedCollection).isNotNull();
  }

  @DisplayName("Test for update method then throw exception")
  @SneakyThrows
  @Test
  void givenEmptyParameters_whenUpdate_thenThrowException() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        NotFoundException.class,
        () -> collectionService.update(new Date(), new Date(), collectionDB.getInternalId()));
    verify(collectionRepository, never()).save(any(Collection.class));
  }

  @DisplayName("Test for set state to inactive method")
  @SneakyThrows
  @Test
  void givenInternalId_whenSetState_thenReturnActiveCollection() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    given(collectionRepository.save(any())).willReturn(collectionDB);
    Collection obtainedCollection = collectionService.setState(collectionDB.getInternalId());
    assertThat(obtainedCollection).isNotNull();
    assertThat(obtainedCollection.getState()).isEqualTo(CollectionEnum.INACTIVE.getValue());
  }

  @DisplayName("Test for set state to active method")
  @SneakyThrows
  @Test
  void givenInternalId_whenSetState_thenReturnInactiveCollection() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    collectionDB.setState(CollectionEnum.INACTIVE.getValue());
    given(collectionRepository.save(any())).willReturn(collectionDB);
    Collection obtainedCollection = collectionService.setState(collectionDB.getInternalId());
    assertThat(obtainedCollection).isNotNull();
    assertThat(obtainedCollection.getState()).isEqualTo(CollectionEnum.ACTIVE.getValue());
  }

  @DisplayName("Test for set state method then return exception")
  @SneakyThrows
  @Test
  void givenEmpty_whenSetState_thenThrowException() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        NotFoundException.class, () -> collectionService.setState(collectionDB.getInternalId()));
    verify(collectionRepository, never()).save(any(Collection.class));
  }

  @DisplayName("Test for obtainAccountByGroupInternalId method")
  @Test
  void obtainAccountByGroupInternalId() {}
}
