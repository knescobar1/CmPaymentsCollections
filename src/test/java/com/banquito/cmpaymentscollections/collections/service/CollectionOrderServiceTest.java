package com.banquito.cmpaymentscollections.collections.service;

import com.banquito.cmpaymentscollections.collections.dao.CollectionOrderRepository;
import com.banquito.cmpaymentscollections.collections.dao.CollectionRepository;
import com.banquito.cmpaymentscollections.collections.enums.CollectionOrderEnum;
import com.banquito.cmpaymentscollections.collections.enums.PaymentWayEnum;
import com.banquito.cmpaymentscollections.collections.exceptions.BadRequestException;
import com.banquito.cmpaymentscollections.collections.exceptions.CSVFileException;
import com.banquito.cmpaymentscollections.collections.exceptions.NotFoundException;
import com.banquito.cmpaymentscollections.collections.model.Collection;
import com.banquito.cmpaymentscollections.collections.model.CollectionCustomer;
import com.banquito.cmpaymentscollections.collections.model.CollectionOrder;
import com.banquito.cmpaymentscollections.collections.model.CollectionServiceOffered;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
class CollectionOrderServiceTest {

  @Mock private CollectionOrderRepository collectionOrderRepository;

  @Mock private CollectionRepository collectionRepository;

  @InjectMocks private CollectionOrderService collectionOrderService;

  private Collection collectionDB;

  private CollectionOrder collectionOrderDB;

  private CollectionOrder collectionOrderNew;

  @BeforeEach
  void setup() {
    BigInteger creditorAccount = new BigInteger("123456789123458");
    BigInteger debtorAccount1 = new BigInteger("14786953200");
    BigInteger debtorAccount2 = new BigInteger("53086089631");
    this.collectionDB =
        Collection.builder()
            .channel("WEB")
            .serviceOffered(
                CollectionServiceOffered.builder()
                    .name("Agua Potable")
                    .description("Cobros de Agua Potable")
                    .build())
            .groupInternalId("5ba4d84b-cd03-448e-9284-ae743d5afe8a")
            .internalId("f3c83802-a16b-417f-a448-43aef9d29234")
            .creditorAccount(creditorAccount)
            .startCollectionDate(new Date())
            .endCollectionDate(new Date())
            .periodicity("Mensual")
            .reference("Cobro Mensual")
            .state("ACT")
            .build();

    this.collectionOrderDB =
        CollectionOrder.builder()
            .collectionId("f3c83802-a16b-417f-a448-43aef9d29234")
            .referenceId("AAA502")
            .internalId("c12e52c8-d263-4336-be71-81530bc0427a")
            .customer(
                CollectionCustomer.builder()
                    .customerId("1726495904")
                    .typeCustomerId("CED")
                    .fullName("Pinta Cofre Carlos Xavier")
                    .build())
            .debtorAccount(debtorAccount1)
            .paymentWay("ACC")
            .amount(BigDecimal.valueOf(35))
            .reference("COBRO DE SERVICIO MENSUAL AGUA")
            .processingState("CRE")
            .state("TP")
            .collectedDate(null)
            .startCollectionDate(new Date())
            .endCollectionDate(new Date())
            .build();

    this.collectionOrderNew =
        CollectionOrder.builder()
            .collectionId("f3c83802-a16b-417f-a448-43aef9d29234")
            .referenceId("AAA503")
            .internalId("c12e52c8-d263-4336-be71-81560bc0332a")
            .customer(
                CollectionCustomer.builder()
                    .customerId("1714104783")
                    .typeCustomerId("CED")
                    .fullName("Dr. Laura Tiffany")
                    .build())
            .debtorAccount(debtorAccount2)
            .paymentWay("ACC")
            .amount(BigDecimal.valueOf(55))
            .reference("COBRO DE SERVICIO MENSUAL AGUA")
            .processingState("CRE")
            .state("TP")
            .collectedDate(null)
            .startCollectionDate(new Date())
            .endCollectionDate(new Date())
            .build();
  }

  @DisplayName("Test for findCollectionById method")
  @Test
  void givenInternalId_whenFindCollectionById_thenReturnCollection() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    Collection collection = collectionOrderService.findCollectionById(collectionDB.getInternalId());
    log.info(
        " La collecion buscada es:{} la encontrada es: {}",
        collectionDB.getInternalId(),
        collection.getInternalId());
    assertThat(collection).isNotNull();
    assertEquals(collectionDB.getInternalId(), collection.getInternalId());
  }

  @DisplayName("Test for findCollectionById method")
  @Test
  void givenInternalId_whenFindByInternalId_thenReturnCollectionOrder() {
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    CollectionOrder collectionOrder =
        collectionOrderService.findByInternalId(collectionDB.getInternalId());
    log.info(
        " La collecion buscada es:{} la encontrada es: {}",
        collectionDB.getInternalId(),
        collectionOrder.getInternalId());
    assertThat(collectionOrder).isNotNull();
    assertEquals(collectionOrderDB.getInternalId(), collectionOrder.getInternalId());
  }

  @DisplayName("Test for findByCollectionId method")
  @Test
  void givenCollectionID_whenFindByCollectionId_thenReturnCollectionOrders() {
    given(collectionOrderRepository.findByCollectionIdOrderByCustomerFullName(any()))
        .willReturn(List.of(collectionOrderDB));
    List<CollectionOrder> collectionOrders =
        collectionOrderService.findByCollectionId(collectionOrderDB.getCollectionId());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for findByReferenceIdAndCollectionId method")
  @Test
  void
      givenReferenceIdAndCollectionId_whenFindByReferenceIdAndCollectionId_thenReturnCollectionOrderList() {
    given(collectionOrderRepository.findByReferenceIdAndCollectionId(any(), any()))
        .willReturn(List.of(collectionOrderDB));
    List<CollectionOrder> collectionOrders =
        collectionOrderService.findByReferenceIdAndCollectionId(
            collectionOrderDB.getReferenceId(), collectionOrderDB.getCollectionId());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for findByStateAndCollectionIdOrderByCustomerName method")
  @Test
  void
      givenStateAndCollectionId_whenFindByStateAndCollectionIdOrderByCustomerName_thenReturnCollectionOrders() {
    given(collectionOrderRepository.findByStateAndCollectionIdOrderByCustomerFullName(any(), any()))
        .willReturn(List.of(collectionOrderDB));
    List<CollectionOrder> collectionOrders =
        collectionOrderService.findByStateAndCollectionIdOrderByCustomerName(
            collectionOrderDB.getState(), collectionOrderDB.getCollectionId());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for findBetweenCollectedDateAndCollectionId method")
  @Test
  void
      givenTwoDatesAndCollectionId_whenFindBetweenCollectedDateAndCollectionId_thenReturnCollectionOrders() {
    given(
            collectionOrderRepository
                .findByCollectedDateBetweenAndCollectionIdOrderByCustomerFullName(
                    any(), any(), any()))
        .willReturn(List.of(collectionOrderDB));
    List<CollectionOrder> collectionOrders =
        collectionOrderService.findBetweenCollectedDateAndCollectionId(
            collectionOrderDB.getCollectedDate(),
            collectionOrderDB.getCollectedDate(),
            collectionOrderDB.getCollectionId());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for findByDebtorAccount method")
  @Test
  void givenDebtorAccount_whenFindByDebtorAccount_thenReturnCollectionOrders() {
    given(collectionOrderRepository.findByDebtorAccountAndStateAndPaymentWay(any(), any(), any()))
        .willReturn(List.of(collectionOrderDB));
    List<CollectionOrder> collectionOrders =
        collectionOrderService.findByDebtorAccount(collectionOrderDB.getDebtorAccount());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for validateCSVFile method")
  @SneakyThrows
  @Test
  void givenCSVFile_whenValidateCSVFile_thenSuccess() {
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test-collections.csv",
            "test-collections.csv",
            "text/csv",
            new FileInputStream("src/test/resources/test-collections.csv"));
    collectionOrderService.validateCSVFile(multipartFile);
  }

  @DisplayName("Test for uploadToPayCsv method then throw exception")
  @SneakyThrows
  @Test
  void givenCSVFile_whenUploadToPayCsv_thenThrowException() {
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test-collections-bad.csv",
            "test-collections-bad.csv",
            "text/csv",
            new FileInputStream("src/test/resources/test-collections-bad.csv"));
    CSVFileException exception =
        Assertions.assertThrows(
            CSVFileException.class, () -> collectionOrderService.validateCSVFile(multipartFile));
    String expectedMessage =
        "No existe el encabezado CONTRAPARTIDA CODIGO DEL CLIENTE en el archivo";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @DisplayName("Test for reportCollection method")
  @SneakyThrows
  @Test
  void givenServiceOfferedStatesAndDates_whenReportCollection_thenReturnCollectionOrders() {
    given(collectionRepository.findByServiceOfferedName(any())).willReturn(List.of(collectionDB));
    given(
            collectionOrderRepository
                .findByCollectionIdInAndStateInAndStartCollectionDateBetweenAndProcessingState(
                    any(), any(), any(), any(), any()))
        .willReturn(List.of(collectionOrderDB));
    List<String> states = new ArrayList<>();
    states.add(collectionOrderDB.getState());
    List<CollectionOrder> collectionOrders =
        collectionOrderService.reportCollection(
            collectionDB.getServiceOffered().getName(),
            states,
            collectionOrderDB.getStartCollectionDate(),
            collectionOrderDB.getStartCollectionDate());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for pay method")
  @SneakyThrows
  @Test
  void givenReferenceIdAndCollectionId_whenPay_thenSuccess() {
    given(collectionOrderRepository.findByReferenceIdAndCollectionId(any(), any()))
        .willReturn(List.of(collectionOrderDB));
    //
    // given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    String value =
        collectionOrderService.pay(
            collectionOrderDB.getReferenceId(), collectionOrderDB.getCollectionId());
    assertThat(value).isNotNull();
    //        assertEquals("pagado", value);

  }

  @DisplayName("Test for processCollectionOrder method")
  @Test
  void givenCollectionId_whenProcessCollectionOrder_thenSuccess() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));

    String value =
        collectionOrderService.processCollectionOrder(collectionOrderDB.getCollectionId());
    assertEquals("Se procesaron con éxito las ordenes de Cobros", value);
  }

  @DisplayName("Test for obtainOrderbyCollectionServiceName method")
  @SneakyThrows
  @Test
  void
      givenServiceOfferedAndReferenceId_whenObtainOrderByCollectionServiceName_thenReturnCollectionOrders() {
    given(collectionRepository.findByServiceOfferedName(any())).willReturn(List.of(collectionDB));
    given(collectionOrderRepository.findByReferenceIdAndCollectionId(any(), any()))
        .willReturn(List.of(collectionOrderDB));
    List<CollectionOrder> collectionOrders =
        collectionOrderService.obtainOrderbyCollectionServiceName(
            collectionDB.getServiceOffered().getName(), collectionOrderDB.getReferenceId());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for obtainByCustomerAndState method")
  @Test
  void givenStateAndCustomerId_whenObtainByCustomerAndState_thenReturnCollectionOrders() {
    given(collectionOrderRepository.findByProcessingStateAndCustomerCustomerId(any(), any()))
        .willReturn(List.of(collectionOrderDB));
    List<CollectionOrder> collectionOrders =
        collectionOrderService.obtainByCustomerAndState(
            collectionOrderDB.getState(), collectionOrderDB.getCustomer().getCustomerId());
    assertThat(collectionOrders).isNotNull();
    assertThat(collectionOrders.contains(collectionOrderDB)).isNotNull();
  }

  @DisplayName("Test for getCollectionOrderRecurrementToPay method")
  @Test
  void
      givenCollectionOrderList_whenGetCollectionOrderRecurrementToPay_thenReturnCollectionOrderList() {
    List<CollectionOrder> orders = List.of(collectionOrderDB, collectionOrderNew);
    given(collectionOrderRepository.findByStateAndPaymentWay(any(), any())).willReturn(orders);
    List<CollectionOrder> obtainedOrders =
        collectionOrderService.getCollectionOrderRecurrementToPay();
    assertThat(obtainedOrders).isNotNull();
    assertThat(obtainedOrders.size()).isEqualTo(2);
  }

  @SneakyThrows
  @DisplayName("Test for create method then can not create order")
  @Test
  void givenExistenceCollectionOrder_whenCreate_thenNoCreateCollectionOrder() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    given(collectionOrderRepository.findByReferenceIdAndCollectionId(any(), any()))
        .willReturn(List.of(collectionOrderDB));
    collectionOrderService.createOne(collectionOrderDB);
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @SneakyThrows
  @DisplayName("Test for create method then throw exception")
  @Test
  void givenEmpty_whenCreate_thenThrowException() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        NotFoundException.class, () -> collectionOrderService.createOne(collectionOrderDB));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @SneakyThrows
  @DisplayName("Test for create method then create order")
  @Test
  void givenExistenceCollectionOrder_whenCreate_thenCreateCollectionOrder() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    given(collectionOrderRepository.findByReferenceIdAndCollectionId(any(), any()))
        .willReturn(List.of(collectionOrderDB, collectionOrderNew));
    collectionOrderDB.setStartCollectionDate(new Date());
    collectionOrderService.createOne(collectionOrderDB);
//    verify(collectionOrderRepository, times(1)).save(any(CollectionOrder.class));
  }

  @SneakyThrows
  @DisplayName("Test for createWithList method then can not create orders")
  @Test
  void givenExistenceCollectionOrder_whenCreateWithList_thenNoCreateCollectionOrders() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.of(collectionDB));
    given(collectionOrderRepository.findByReferenceIdAndCollectionId(any(), any()))
        .willReturn(List.of(collectionOrderDB));
    collectionOrderService.createWithList(List.of(collectionOrderDB));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @SneakyThrows
  @DisplayName("Test for createWithList method then throw exception")
  @Test
  void givenEmpty_whenCreateWithList_thenThrowException() {
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        NotFoundException.class,
        () -> collectionOrderService.createWithList(List.of(collectionOrderDB)));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @DisplayName("Test for collectedAmount method then success")
  @SneakyThrows
  @Test
  void givenInternalId_whenCollectedAmount_thenSuccess() {
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    given(collectionOrderRepository.save(any())).willReturn(collectionOrderDB);
    collectionOrderService.collectedAmount(collectionOrderDB.getInternalId());
    verify(collectionOrderRepository, times(1)).save(any(CollectionOrder.class));
  }

  @DisplayName("Test for collectedAmount method then throw exception")
  @Test
  void givenEmpty_whenCollectedAmount_thenThrowException() {
    given(collectionOrderRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        NotFoundException.class,
        () -> collectionOrderService.collectedAmount(collectionOrderDB.getInternalId()));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @DisplayName("Test for readyCollection method then success")
  @SneakyThrows
  @Test
  void givenIdList_whenReadyCollection_thenSuccess() {
    List<String> ids = List.of("c12e52c8-d263-4336-be71-81530bc0427a");
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    given(collectionOrderRepository.save(any())).willReturn(collectionOrderDB);
    collectionOrderService.readyCollection(ids);
    verify(collectionOrderRepository, times(1)).save(any(CollectionOrder.class));
  }

  @DisplayName("Test for readyCollection method then throw exception")
  @SneakyThrows
  @Test
  void givenIdList_whenReadyCollection_thenThrowException() {
    List<String> ids = List.of("1");
    given(collectionOrderRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(NotFoundException.class, () -> collectionOrderService.readyCollection(ids));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @DisplayName("Test for uploadToPay method then throw exception")
  @SneakyThrows
  @Test
  void givenEmptyCollectionId_whenUploadToPayCSV_thenThrowException() {
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test-collections.csv",
            "test-collections.csv",
            "text/csv",
            new FileInputStream("src/test/resources/test-collections.csv"));
    given(collectionRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        NotFoundException.class,
        () -> collectionOrderService.uploadToPayCsv(multipartFile, collectionDB.getCollectionId()));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @DisplayName("Test for updateCollectionOrder method then return updated collection order")
  @SneakyThrows
  @Test
  void givenCollectionOrder_whenUpdateCollectionOrder_thenReturnUpdatedCollectionOrder() {
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    given(collectionOrderRepository.save(any())).willReturn(collectionOrderDB);
    CollectionOrder obtainedOrder = collectionOrderService.updateCollectionOrder(collectionOrderDB);
    assertThat(obtainedOrder).isNotNull();
  }

  @DisplayName("Test for updateCollectionOrder method then return exception collection order empty")
  @SneakyThrows
  @Test
  void givenEmptyCollectionOrder_whenUpdateCollectionOrder_thenReturnException() {
    given(collectionOrderRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        CSVFileException.class,
        () -> collectionOrderService.updateCollectionOrder(collectionOrderDB));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @DisplayName(
      "Test for updateCollectionOrder method then return exception collection order already paid")
  @SneakyThrows
  @Test
  void givenPaidCollectionOrder_whenUpdateCollectionOrder_thenReturnException() {
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    collectionOrderDB.setState(CollectionOrderEnum.PAID.getValue());
    assertThrows(
        BadRequestException.class,
        () -> collectionOrderService.updateCollectionOrder(collectionOrderDB));
    verify(collectionOrderRepository, never()).save(any(CollectionOrder.class));
  }

  @DisplayName("Test for deleteCollectionOrder method then delete order")
  @SneakyThrows
  @Test
  void givenCollectionOrderId_whenDeleteCollectionOrder_thenDeleteOrder() {
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    willDoNothing().given(collectionOrderRepository).delete(collectionOrderDB);
    collectionOrderService.deleteCollectionOrder(collectionOrderDB.getInternalId());
    verify(collectionOrderRepository, times(1)).delete(any());
  }

  @DisplayName("Test for deleteCollectionOrder method then return exception collection order empty")
  @SneakyThrows
  @Test
  void givenEmptyCollectionOrder_whenDeleteCollectionOrder_thenReturnException() {
    given(collectionOrderRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        NotFoundException.class,
        () -> collectionOrderService.deleteCollectionOrder(collectionOrderDB.getInternalId()));
    verify(collectionOrderRepository, never()).delete(any(CollectionOrder.class));
  }

  @DisplayName(
      "Test for deleteCollectionOrder method then return exception collection order already paid")
  @SneakyThrows
  @Test
  void givenPaidCollectionOrder_whenDeleteCollectionOrder_thenReturnException() {
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    collectionOrderDB.setState(CollectionOrderEnum.PAID.getValue());
    assertThrows(
        BadRequestException.class,
        () -> collectionOrderService.deleteCollectionOrder(collectionOrderDB.getInternalId()));
    verify(collectionOrderRepository, never()).delete(any(CollectionOrder.class));
  }

  @DisplayName("Test for changePaymentWay method then change payment way")
  @Test
  void givenCollectionOrderId_whenChangePaymentWay_thenChangePaymentWay() {
    given(collectionOrderRepository.findByInternalId(any()))
        .willReturn(Optional.of(collectionOrderDB));
    collectionOrderDB.setPaymentWay(PaymentWayEnum.MANUAL.getValue());
    given(collectionOrderRepository.save(any())).willReturn(collectionOrderDB);
    collectionOrderService.changePaymentWay(collectionOrderDB.getInternalId());
    verify(collectionOrderRepository, times(1)).save(any());
  }
}
