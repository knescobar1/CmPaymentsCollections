package com.banquito.cmpaymentscollections.payments.service;

import com.banquito.cmpaymentscollections.payments.dao.PaymentOrderRepository;
import com.banquito.cmpaymentscollections.payments.dao.PaymentRepository;
import com.banquito.cmpaymentscollections.payments.enums.StateEnum;
import com.banquito.cmpaymentscollections.payments.enums.StateOrderEnum;
import com.banquito.cmpaymentscollections.payments.exception.CSVFileException;
import com.banquito.cmpaymentscollections.payments.exception.PaymentServiceException;
import com.banquito.cmpaymentscollections.payments.model.Payment;
import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
class PaymentOrderServiceTest {

  @Mock private PaymentRepository paymentRepository;
  @Mock private PaymentOrderRepository paymentOrderRepository;

  @InjectMocks private PaymentOrderService paymentOrderService;
  private Payment payment1;
  private PaymentOrder order1;
  private PaymentOrder order2;

  @BeforeEach
  void setup() {

    this.payment1 =
        Payment.builder()
            .internalId("1")
            .groupInternalId("123")
            .serviceName("Pago nomina")
            .serviceDescription("Pago nomina mes de julio")
            .state(StateEnum.PENDING.getValue())
            .debtorAccount(BigInteger.valueOf(1234567890))
            .creationDate(new Date())
            .lastModifiedDate(null)
            .processDate(new Date())
            .records(0)
            .paidRecords(0)
            .failedRecords(0)
            .totalPaymentValue(BigDecimal.valueOf(0))
            .paidValue(BigDecimal.valueOf(0))
            .failedValue(BigDecimal.valueOf(0))
            .journalId("1")
            .build();

    this.order1 =
        PaymentOrder.builder()
            .internalId("1")
            .referenceId("000016")
            .paymentId("1")
            .creditorIdentification("1303801540")
            .creditorIdentificationType("CED")
            .creditorName("Belson David Andres")
            .creditorAccount(BigInteger.valueOf(955534718))
            .creditorAccountType("AHO")
            .creditorEmail("lantonio54@live.com")
            .amount(BigDecimal.valueOf(150.00))
            .state(StateEnum.PENDING.getValue())
            .description("PAGO ANTICIPO EMPLEADOS AL 31/07/2022")
            .paymentDate(new Date())
            .build();
    this.order2 =
        PaymentOrder.builder()
            .internalId("2")
            .referenceId("000017")
            .paymentId("1")
            .creditorIdentification("0703269860")
            .creditorIdentificationType("CED")
            .creditorName("Pazmiño Maria Jose")
            .creditorAccount(BigInteger.valueOf(326540072))
            .creditorAccountType("AHO")
            .creditorEmail("gabalex225@gmail.com")
            .amount(BigDecimal.valueOf(150.00))
            .state(StateEnum.PENDING.getValue())
            .description("PAGO ANTICIPO EMPLEADOS AL 31/07/2022")
            .paymentDate(new Date())
            .build();
  }

  @DisplayName("JUnit test for createPaymentOrder method")
  @Test
  void givenPaymentOrderAndPaymentId_whenCreatePaymentOrder_thenReturnPaymentOrder() {
    given(paymentRepository.findByInternalId(any())).willReturn(Optional.of(payment1));
    given(paymentOrderRepository.save(any())).willReturn(order1);
    PaymentOrder savedOrder = paymentOrderService.create(order1, payment1.getInternalId());
    log.info("{}", savedOrder);
    assertThat(savedOrder).isNotNull();
  }

  @DisplayName("JUnit test for createPaymentOrder method throws exception")
  @Test
  void givenPaymentOrderAndEmptyPaymentId_whenCreatePaymentOrder_thenReturnException() {
    given(paymentRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        PaymentServiceException.class,
        () -> paymentOrderService.create(order1, payment1.getInternalId()));
    verify(paymentOrderRepository, never()).save(any(PaymentOrder.class));
  }

  @DisplayName("JUnit test for createOrders method")
  @Test
  void givenPaymentOrderListAndPaymentId_whenCreateOrders_thenNothing() {
    List<PaymentOrder> orders = new ArrayList<>();
    orders.add(order1);
    orders.add(order2);
    given(paymentRepository.findByInternalId(any())).willReturn(Optional.of(payment1));
    given(paymentOrderRepository.save(order1)).willReturn(order1);
    given(paymentOrderRepository.save(order2)).willReturn(order2);
    paymentOrderService.createOrders(orders, payment1.getInternalId());
    verify(paymentOrderRepository, times(2)).save(any(PaymentOrder.class));
  }

  @DisplayName("JUnit test for deletePaymentOrder method")
  @Test
  void givenPaymentOrderId_whenDeletePaymentOrder_thenReturnDeletedPaymentOrder() {
    given(paymentOrderRepository.findByInternalId(any())).willReturn(Optional.of(order1));
    given(paymentOrderRepository.save(any())).willReturn(order1);
    order1.setState(StateOrderEnum.DELETED.getValue());
    PaymentOrder deletedPaymentOrder = paymentOrderService.deleteOrder(order1.getPaymentId());
    log.info(deletedPaymentOrder.toString());
    assertThat(deletedPaymentOrder.getState()).isEqualTo(StateOrderEnum.DELETED.getValue());
  }

  @DisplayName("JUnit test for obtainPaymentOrderById method")
  @Test
  void givenPaymentOrderId_whenObtainPaymentOrderById_thenReturnPaymentOrder() {
    given(paymentOrderRepository.findByInternalId(any())).willReturn(Optional.of(order1));
    PaymentOrder obtainedOrder = paymentOrderService.obtainById(order1.getInternalId());
    assertThat(obtainedOrder).isNotNull();
  }

  @DisplayName("JUnit test for obtainPaymentOrderById method return null")
  @Test
  void givenEmptyPaymentOrderId_whenObtainPaymentOrderById_thenReturnNull() {
    given(paymentOrderRepository.findByInternalId(any())).willReturn(Optional.empty());
    PaymentOrder obtainedOrder = paymentOrderService.obtainById(order1.getInternalId());
    assertThat(obtainedOrder).isNull();
  }

  @DisplayName("JUnit test for ValidateCSVFile method")
  @SneakyThrows
  @Test
  void givenCSV_whenValidateCSVFile_thenSuccess() {
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test.csv", "test.csv", "text/csv", new FileInputStream("src/test/resources/test.csv"));
    paymentOrderService.validateCSVFile(multipartFile);
  }

  @DisplayName("JUnit test for ValidateCSVFile method throws exception 'cause no header")
  @SneakyThrows
  @Test
  void givenCSVWithNoHeaders_whenValidateCSVFile_thenReturnException() {
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test.csv",
            "test.csv",
            "text/csv",
            new FileInputStream("src/test/resources/testNoHeader.csv"));
    CSVFileException exception =
        assertThrows(
            CSVFileException.class, () -> paymentOrderService.validateCSVFile(multipartFile));
    String expectedMessage =
        "No existe el encabezado CONTRAPARTIDA CODIGO DE BENEFICARIO Y/O EMPLEADO en el archivo";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @DisplayName("JUnit test for ValidateCSVFile method throws exception 'cause no data")
  @SneakyThrows
  @Test
  void givenCSVWithNoData_whenValidateCSVFile_thenReturnException() {
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test.csv",
            "test.csv",
            "text/csv",
            new FileInputStream("src/test/resources/testNoData.csv"));
    CSVFileException exception =
        assertThrows(
            CSVFileException.class, () -> paymentOrderService.validateCSVFile(multipartFile));
    String expectedMessage = "El dato";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @DisplayName("JUnit test for CreateOrdersByCSV method")
  @SneakyThrows
  @Test
  void givenCSVAndPaymentId_whenCreateOrdersByCSV_thenReturnPaymentOrderList() {
    List<PaymentOrder> orders = new ArrayList<>();
    orders.add(order1);
    orders.add(order2);
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test.csv", "test.csv", "text/csv", new FileInputStream("src/test/resources/test.csv"));
    given(paymentRepository.findByInternalId(any())).willReturn(Optional.of(payment1));
    given(paymentRepository.save(any())).willReturn(payment1);
    given(paymentOrderRepository.saveAll(any())).willReturn(orders);
    List<PaymentOrder> obtainedOrders =
        paymentOrderService.createOrdersByCSV(multipartFile, payment1.getInternalId());
    verify(paymentRepository, times(1)).save(any(Payment.class));
    assertThat(obtainedOrders).isNotNull();
  }

  @DisplayName("JUnit test for CreateOrdersByCSV method throws exception")
  @SneakyThrows
  @Test
  void givenCSVAndEmptyPaymentId_whenCreateOrdersByCSV_thenReturnException() {
    MultipartFile multipartFile =
        new MockMultipartFile(
            "test.csv", "test.csv", "text/csv", new FileInputStream("src/test/resources/test.csv"));
    given(paymentRepository.findByInternalId(any())).willReturn(Optional.empty());
    assertThrows(
        PaymentServiceException.class,
        () -> paymentOrderService.createOrdersByCSV(multipartFile, payment1.getInternalId()));
    verify(paymentRepository, never()).save(any(Payment.class));
    verify(paymentOrderRepository, never()).save(any(PaymentOrder.class));
  }

  @DisplayName("JUnit test for ApproveOrder method")
  @Test
  void givenPaymentOrderId_whenApproveOrder_thenReturnApprovedPaymentOrder() {
    given(paymentOrderRepository.findByInternalId(any())).willReturn(Optional.of(order1));
    given(paymentOrderRepository.save(any())).willReturn(order1);
    order1.setState(StateEnum.APPROVED.getValue());
    PaymentOrder obtainedOrder = paymentOrderService.approveOrder(order1.getPaymentId());
    log.info("{}", obtainedOrder);
    assertThat(obtainedOrder.getState()).isEqualTo(StateEnum.APPROVED.getValue());
  }

  @DisplayName("JUnit test for ObtainOrdersByPaymentId method")
  @Test
  void givenPaymentId_whenObtainOrdersByPaymentId_thenReturnPaymentOrderList() {
    List<PaymentOrder> orders = new ArrayList<>();
    orders.add(order1);
    orders.add(order2);
    given(paymentOrderRepository.findByPaymentId(any())).willReturn(orders);
    List<PaymentOrder> obtainedOrders =
        paymentOrderService.obtainOrdersByPaymentId(payment1.getInternalId());
    assertThat(obtainedOrders).isNotNull();
    assertThat(obtainedOrders.size()).isEqualTo(2);
  }

  @DisplayName("JUnit test for ObtainOrdersByFilter method")
  @Test
  void givenGroupInternalId_whenObtainOrdersByFilter_thenReturnPaymentOrderList() {
    given(paymentRepository.findByGroupInternalIdOrderByCreationDate(any()))
        .willReturn(List.of(payment1));
    given(paymentOrderRepository.findOrdersByProperties(any(), any(), any(), any(), any()))
        .willReturn(List.of(order1));
    List<PaymentOrder> obtainedOrders =
        paymentOrderService.obtainOrdersByFilter("1", null, null, null, "123");
    assertThat(obtainedOrders).isNotNull();
    assertThat(obtainedOrders.size()).isEqualTo(1);
  }
}
