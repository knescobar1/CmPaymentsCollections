package com.banquito.cmpaymentscollections.payments.service;

import com.banquito.cmpaymentscollections.payments.enums.SequenceEnum;
import com.banquito.cmpaymentscollections.payments.model.Sequence;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
@ExtendWith({MockitoExtension.class})
class SequenceServiceTest {

  @Mock private MongoOperations mongoOperations;

  @InjectMocks private SequenceService sequenceService;

  private String sequenceTN;
  private String sequenceDN;
  private String sequenceEO;

  @BeforeEach
  void setUp() {
    this.sequenceTN = "TN0000001";
    this.sequenceDN = "DN0000001";
    this.sequenceEO = "EO0000001";
  }

  @DisplayName("JUnit test to get Next Transaction Number Sequence")
  @Test
  void getNextTN() {
    given(
            mongoOperations.findAndModify(
                any(Query.class),
                any(Update.class),
                any(FindAndModifyOptions.class),
                eq(Sequence.class)))
        .willReturn(new Sequence().setValue(1L).setId(SequenceEnum.TRANSACTION_NUMBER.getPrefix()));
    String obtainedSequence = sequenceService.getNextTN();
    assertThat(obtainedSequence).isEqualTo(sequenceTN);
    verify(mongoOperations, times(1))
        .findAndModify(
            any(Query.class),
            any(Update.class),
            any(FindAndModifyOptions.class),
            eq(Sequence.class));
  }

  @DisplayName("JUnit test to get Next Document Number Sequence")
  @Test
  void getNextDN() {
    given(
            mongoOperations.findAndModify(
                any(Query.class),
                any(Update.class),
                any(FindAndModifyOptions.class),
                eq(Sequence.class)))
        .willReturn(new Sequence().setValue(1L).setId(SequenceEnum.DOCUMENT_NUMBER.getPrefix()));
    String obtainedSequence = sequenceService.getNextDN();
    assertThat(obtainedSequence).isEqualTo(sequenceDN);
    verify(mongoOperations, times(1))
        .findAndModify(
            any(Query.class),
            any(Update.class),
            any(FindAndModifyOptions.class),
            eq(Sequence.class));
  }

  @DisplayName("JUnit test to get Next Document Number Sequence")
  @Test
  void givenNull_whenGetNextDN() {
    given(
            mongoOperations.findAndModify(
                any(Query.class),
                any(Update.class),
                any(FindAndModifyOptions.class),
                eq(Sequence.class)))
        .willReturn(null);
    String obtainedSequence = sequenceService.getNextDN();
    assertThat(obtainedSequence).isEqualTo(sequenceDN);
    verify(mongoOperations, times(1))
        .findAndModify(
            any(Query.class),
            any(Update.class),
            any(FindAndModifyOptions.class),
            eq(Sequence.class));
  }

  @DisplayName("JUnit test to get Next External Operation Sequence")
  @Test
  void getNextEO() {
    given(
            mongoOperations.findAndModify(
                any(Query.class),
                any(Update.class),
                any(FindAndModifyOptions.class),
                eq(Sequence.class)))
        .willReturn(new Sequence().setValue(1L).setId(SequenceEnum.EXTERNAL_OPERATION.getPrefix()));
    String obtainedSequence = sequenceService.getNextEO();
    assertThat(obtainedSequence).isEqualTo(sequenceEO);
    verify(mongoOperations, times(1))
        .findAndModify(
            any(Query.class),
            any(Update.class),
            any(FindAndModifyOptions.class),
            eq(Sequence.class));
  }
}
