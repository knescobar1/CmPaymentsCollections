package com.banquito.cmpaymentscollections.payments.service;

import com.banquito.cmpaymentscollections.payments.dao.PaymentRepository;
import com.banquito.cmpaymentscollections.payments.dto.AccountDTO;
import com.banquito.cmpaymentscollections.payments.enums.StateEnum;
import com.banquito.cmpaymentscollections.payments.model.Payment;
import com.banquito.cmpaymentscollections.payments.model.PaymentOrder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@Slf4j
@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {

  @Mock private PaymentRepository paymentRepository;
  // @Mock private PaymentOrderRepository paymentOrderRepository;
  @Mock private RestTemplate restTemplate;

  @InjectMocks private PaymentService paymentService;
  private Payment payment1;
  private Payment payment2;
  private PaymentOrder order1;
  private AccountDTO account;

  @BeforeEach
  void setup() {
    this.payment1 =
        Payment.builder()
            .internalId("1")
            .groupInternalId("123")
            .serviceName("Nomina")
            .serviceDescription("Julio")
            .state(StateEnum.PENDING.getValue())
            .debtorAccount(BigInteger.valueOf(1234567890))
            .creationDate(new Date())
            .lastModifiedDate(null)
            .processDate(new Date())
            .records(1)
            .paidRecords(1)
            .failedRecords(0)
            .totalPaymentValue(BigDecimal.valueOf(100.00))
            .paidValue(BigDecimal.valueOf(100.00))
            .failedValue(BigDecimal.valueOf(0))
            .journalId("1")
            .build();

    this.payment2 =
        Payment.builder()
            .internalId("2")
            .groupInternalId("124")
            .serviceName("Servicios")
            .serviceDescription("Agosto")
            .state(StateEnum.PENDING.getValue())
            .debtorAccount(BigInteger.valueOf(1234567891))
            .creationDate(new Date())
            .lastModifiedDate(null)
            .processDate(new Date())
            .records(0)
            .paidRecords(0)
            .failedRecords(0)
            .totalPaymentValue(BigDecimal.valueOf(0))
            .paidValue(BigDecimal.valueOf(0))
            .failedValue(BigDecimal.valueOf(0))
            .journalId("2")
            .build();

    this.order1 =
        PaymentOrder.builder()
            .internalId("1")
            .referenceId("000016")
            .paymentId("1")
            .creditorIdentification("1303801540")
            .creditorIdentificationType("CED")
            .creditorName("Belson David Andres")
            .creditorAccount(BigInteger.valueOf(955534718))
            .creditorAccountType("AHO")
            .creditorEmail("lantonio54@live.com")
            .amount(BigDecimal.valueOf(150.00))
            .state(StateEnum.PENDING.getValue())
            .description("PAGO ANTICIPO EMPLEADOS AL 31/07/2022")
            .paymentDate(new Date())
            .build();

    this.account =
        AccountDTO.builder()
            .accountNumber(BigInteger.valueOf(1234567890))
            .familyId("1")
            .customerGroupId("1234")
            .accountType("AHO")
            .openingDate(new Date())
            .maturityDate(new Date())
            .balance(BigDecimal.valueOf(1000.00))
            .state("ACTIVE")
            .build();
  }

  @DisplayName("JUnit test for SavePayment method")
  @Test
  void givenPayment_whenSavePayment_thenReturnPayment() {
    given(paymentRepository.save(payment1)).willReturn(payment1);
    Payment savedPayment = paymentService.create(payment1);
    log.info("{}", savedPayment);
    assertThat(savedPayment).isNotNull();
  }

  @DisplayName("JUnit test for ObtainPaymentById method")
  @Test
  void givenPaymentId_whenObtainPaymentById_thenReturnPayment() {
    given(paymentRepository.findByInternalId(any())).willReturn(Optional.of(payment1));
    Payment obtainedPayment = paymentService.obtainByInternalId(payment1.getInternalId());
    log.info("{}", obtainedPayment);
    assertThat(obtainedPayment).isNotNull();
  }

  @DisplayName("JUnit test for ObtainPaymentById method return null")
  @Test
  void givenPaymentId_whenObtainPaymentById_thenReturnNull() {
    given(paymentRepository.findByInternalId(any())).willReturn(Optional.empty());
    Payment obtainedPayment = paymentService.obtainByInternalId(payment1.getInternalId());
    log.info("{}", obtainedPayment);
    assertThat(obtainedPayment).isNull();
  }

  @DisplayName("JUnit test for ObtainPaymentByProperties method")
  @Test
  void givenPaymentList_whenObtainPaymentByProperties_thenReturnPaymentList() {
    given(
            paymentRepository.findPaymentsByProperties(
                any(), any(), any(), any(), any(), any(), any()))
        .willReturn(List.of(payment2));
    List<Payment> obtainedPayments =
        paymentService.obtainPaymentsByFilter("124", null, null, null, null, null, null);
    assertThat(obtainedPayments).isNotNull();
    assertThat(obtainedPayments.size()).isEqualTo(1);
  }

  @DisplayName("JUnit test for ObtainPaymentByPropertiesState method")
  @Test
  void givenPaymentList_whenObtainPaymentByPropertiesState_thenReturnPaymentList() {
    given(paymentRepository.findPaymentsByPropertiesState(any(), any(), any(), any(), any()))
        .willReturn(List.of(payment1, payment2));
    List<Payment> obtainedPayments =
        paymentService.obtainPaymentsByState(null, "PEN", null, null, null);
    assertThat(obtainedPayments).isNotNull();
    assertThat(obtainedPayments.size()).isEqualTo(2);
  }
}
