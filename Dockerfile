FROM openjdk:11.0.13-jdk-oracle

VOLUME /tmp
ADD /target/cm-payments-collections-1.jar app.jar

ENTRYPOINT ["java","-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=50251,quiet=y", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]